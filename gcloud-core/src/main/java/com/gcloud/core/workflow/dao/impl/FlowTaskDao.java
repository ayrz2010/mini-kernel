package com.gcloud.core.workflow.dao.impl;

import org.springframework.stereotype.Repository;

import com.gcloud.core.workflow.dao.IFlowTaskDao;
import com.gcloud.core.workflow.entity.FlowTask;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
@Repository
public class FlowTaskDao  extends JdbcBaseDaoImpl<FlowTask, Long> implements IFlowTaskDao{

}
