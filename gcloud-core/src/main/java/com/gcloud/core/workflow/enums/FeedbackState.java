package com.gcloud.core.workflow.enums;

public enum FeedbackState {
	SKIP,
	SUCCESS,
	SUCCESS_Y,
    SUCCESS_N,
	FAILURE;
}
