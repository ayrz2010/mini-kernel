package com.gcloud.core.workflow.rest;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gcloud.core.workflow.engine.WorkFlowEngine;
import com.gcloud.core.workflow.model.FeedBackParam;

@RestController
@RequestMapping("/")
public class WorkflowController {
	@RequestMapping(value = "/workflow/feedback.do")
	public void feedback(@Validated FeedBackParam param){
		WorkFlowEngine.feedbackHandler(param.getTaskId(), param.getStatus(), param.getCode());
	}
}
