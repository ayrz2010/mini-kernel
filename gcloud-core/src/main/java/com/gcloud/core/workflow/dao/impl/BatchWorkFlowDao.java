package com.gcloud.core.workflow.dao.impl;

import org.springframework.stereotype.Repository;

import com.gcloud.core.workflow.dao.IBatchWorkFlowDao;
import com.gcloud.core.workflow.entity.BatchWorkFlow;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
@Repository
public class BatchWorkFlowDao extends JdbcBaseDaoImpl<BatchWorkFlow, Long> implements IBatchWorkFlowDao{

}
