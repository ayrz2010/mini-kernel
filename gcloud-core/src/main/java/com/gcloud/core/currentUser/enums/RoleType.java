package com.gcloud.core.currentUser.enums;

public enum RoleType {
	SUPER_ADMIN("superadmin", "超级管理员"),
	TENANT_ADMIN("tenantadmin", "租户管理员"),
	ORDINARY_USER("ordinary", "普通用户");
	
	
	private String roleId;
	private String roleName;
	
	RoleType(String roleId, String roleName){
		this.roleId = roleId;
		this.roleName = roleName;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	
}
