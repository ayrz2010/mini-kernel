package com.gcloud.core.cache.redis.template;

import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.util.SerializeUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.ShardedJedisPool;

import java.util.Map;
import java.util.Set;

@Slf4j
public class JedisClientTemplate {
    @Autowired
    private ShardedJedisPool shardedJedisPool;

    public void setObject(String key, Object value) throws GCloudException {

        byte[] keyByte = key.getBytes();
        byte[] valueByte = SerializeUtils.serializeToByte(value);
        set(keyByte, valueByte);

    }

    public Object getObject(String key) throws GCloudException {

        byte[] keyByte = key.getBytes();
        byte[] valueByte = get(keyByte);
        Object obj = null;
        if (valueByte != null) {
            obj = SerializeUtils.unSerializeFromByte(valueByte);
        }
        return obj;

    }

    /**
     * 设置单个值
     *
     * @param key
     * @param value
     * @param nxxx  NX|XX, NX -- Only set the key if it does not already exist. XX -- Only set the key
     *              if it already exist.
     * @param expx  EX|PX, expire time units: EX = seconds; PX = milliseconds
     * @param time  expire time in the units of <code>expx</code>
     * @return Status code reply
     */
    public String set(String key, String value, String nxxx, String expx, long time) {
        String result = null;
        try (ShardedJedis shardedJedis = shardedJedisPool.getResource()) {
            result = shardedJedis.set(key, value, nxxx, expx, time);
        }
        return result;
    }

    /**
     * 设置单个值
     *
     * @param key
     * @param value
     * @return
     */
    public String set(String key, String value) {
        String result = null;
        try (ShardedJedis shardedJedis = shardedJedisPool.getResource()) {
            result = shardedJedis.set(key, value);
        }
        return result;
    }

    /**
     * 获取单个值
     *
     * @param key
     * @return
     */
    public String get(String key) {
        String result = null;
        try (ShardedJedis shardedJedis = shardedJedisPool.getResource()) {
            result = shardedJedis.get(key);

        }
        return result;
    }

    public String set(byte[] key, byte[] value) throws GCloudException {
        String result = null;
        try (ShardedJedis shardedJedis = shardedJedisPool.getResource()) {
            result = shardedJedis.set(key, value);
        }
        return result;
    }

    public byte[] get(byte[] key) throws GCloudException {
        byte[] result = null;
        try (ShardedJedis shardedJedis = shardedJedisPool.getResource()) {
            result = shardedJedis.get(key);

        }
        return result;
    }

    public String hget(String key, String field) {
        String result = null;
        try (ShardedJedis shardedJedis = shardedJedisPool.getResource()) {
            result = shardedJedis.hget(key, field);
        }
        return result;
    }

    public Long hset(String key, String field, String value) {
        Long result = null;
        try (ShardedJedis shardedJedis = shardedJedisPool.getResource()) {
            result = shardedJedis.hset(key, field, value);
        }
        return result;
    }

    public String hmset(String key, Map<String, String> mapV) {
        String result = null;
        try (ShardedJedis shardedJedis = shardedJedisPool.getResource()) {
            result = shardedJedis.hmset(key, mapV);
        }
        return result;
    }

    public Boolean exists(String key) throws GCloudException {
        Boolean result = false;
        try (ShardedJedis shardedJedis = shardedJedisPool.getResource()) {
            result = shardedJedis.exists(key);
        }
        return result;
    }


    /**
     * @param key
     * @param value
     * @param time  超时时间，单位：秒
     * @return 如返回1，则该客户端获得锁，把lock.foo的键值设置为时间值表示该键已被锁定，该客户端最后可以通过DEL lock.foo来释放该锁。
     * 如返回0，表明该锁已被其他客户端取得，这时我们可以先返回或进行重试等对方完成或等待锁超时。
     * @throws GCloudException
     * @Title: 非空才新增，并且加上超时时间
     * @Description: 以NX结尾，NX是Not eXists的缩写，如SETNX命令就应该理解为：SET if Not eXists
     * @date 2016-10-18 上午11:37:35
     */
    public Long setnx(String key, String value, long time) throws GCloudException {
        Long result = null;
        try (ShardedJedis shardedJedis = shardedJedisPool.getResource()) {
            String res = shardedJedis.set(key, value, "NX", "PX", time);
            if (StringUtils.isNotBlank(res) && res.equalsIgnoreCase("ok")) {
                result = 1l;
            } else {
                result = 0l;
            }
        }
        return result;
    }

    public Long del(String key) throws GCloudException {
        Long result = null;
        try (ShardedJedis shardedJedis = shardedJedisPool.getResource()) {
            result = shardedJedis.del(key);

        }
        return result;
    }

    public Long hdel(String key, String field) throws GCloudException {
        Long result = null;
        try (ShardedJedis shardedJedis = shardedJedisPool.getResource()) {
            result = shardedJedis.hdel(key, field);

        }
        return result;
    }

    public Long expire(String key, int seconds) throws GCloudException {
        Long result = null;
        try (ShardedJedis shardedJedis = shardedJedisPool.getResource()) {
            result = shardedJedis.expire(key, seconds);

        }
        return result;
    }

    private Jedis getShard(ShardedJedis shardedJedis, String key) throws GCloudException {
        Jedis result = null;
        if (shardedJedis == null) {
            return result;
        }
        return shardedJedis.getShard(key);
        /*
         * ShardedJedis shardedJedis = redisDataSource.getRedisClient(); Jedis
         * result = null; if (shardedJedis == null) { return result; } boolean
         * broken = false; try { result = shardedJedis.getShard(key); } catch
         * (Exception e) { log.error(e.getMessage(), e); broken = true; }
         * finally { redisDataSource.returnResource(shardedJedis, broken); }
         * return result;
         */
    }

    public Set<String> keys(String key) throws GCloudException {
        Exception ex = null;
        int i = 1;
        int count = 5; // redis重连次数
        while (i <= count) {
            try (ShardedJedis shardedJedis = shardedJedisPool.getResource()) {
                return getShard(shardedJedis, key).keys(key);
            } catch (Exception e) {
                ex = e;
                log.error(String.format("keys 重连第%s次 成功，key：%s", i, key), e);
                i++;
            }
        }

        if (ex != null) {
            log.error(String.format("keys 重连%s次 失败，key：%s", i, key), ex);
            throw new GCloudException("数据连接繁忙，请稍等。");
        } else {
            throw new GCloudException("获取keys失败，未知错误");
        }
    }

    public Map<String, String> hgetAll(String key) {
        Map<String, String> result = null;
        try (ShardedJedis shardedJedis = shardedJedisPool.getResource()) {
            result = shardedJedis.hgetAll(key);
        }
        return result;
    }
}
