package com.gcloud.core.cache.enums;

public enum CacheType {
	ACTIVE_INFO,   // 服务激活信息
	NODE_INFO,     // 节点信息
	TIMER,         // 定时器相关数据
	TOKEN_USER,
	SIGN_USER
}
