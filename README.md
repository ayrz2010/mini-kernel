# 中国云OS最小内核

#### 介绍
中国云操作系统最小内核开源项目，基于中国云标准API。
中国云OS最小内核定位于打造成一个标准的最小化核心的基础云平台，功能组件包括计算、存储、网络、认证。
 
**主要功能模块** 
- 弹性计算服务：

1. 地域管理
2. 虚拟机管理
3. 磁盘管理
4. 镜像管理
5. 快照管理
6. 专有网络管理
7. 外部网络管理
8. 路由管理
9. 交换机管理
10. 安全组客理
11. 网卡管理
12. 弹性公网IP管理

- 负载均衡服务：

1. 负载均衡实例管理
2. 监听器管理
3. 后端服务管理
4. 调度策略管理
5. 健康检查


#### 软件架构
1. 后端：
- 基础框架：Spring Boot 1.5.13.RELEASE
- 持久层框架：gcloud-framework-db-7.1.0
- 安全框架：gcloud-service-identity-8.0.0
- 数据库连接池：阿里巴巴Druid 1.0.26
- 消息队列：rabbitmq
- 缓存框架：redis
- 日志打印：log4j
- 其他：fastjson，quartz, lombok（简化代码）等

2. 开发环境：
- 语言：Java 8
- IDE(JAVA)： Eclipse安装lombok插件 或者 IDEA
- 依赖管理：Maven
- 数据库：Mariadb
- 缓存：Redis


#### 安装教程

1. [详见本项目附件“云操作系统最新内核编译部署V1.0.docx”](https://gitee.com/osgcloud/oskernel/attach_files/250645/download)

#### 使用说明

 **后台开发环境和依赖** 

1. java
2. maven
3. jdk8
4. mariadb
5. 数据库脚步：gcloud\gcloud-boot\sql\gcloud_controller_quartz.sql gcloud\gcloud-boot\sql\gcloud_controller.sql
6. 默认登录账号： admin/gcloud123 （源码版本免登录）


 **项目下载和运行** 
1. 拉取项目代码
直接下载本项目，或者远程拉取git clone http://113.105.131.176:29780/gcloud/g-cloud-8.0.git
cd g-cloud-8.0/gcloud
2. 编译
mvn clean;mvn package;
3. 运行
cd  gcloud/gcloud-boot/target/
nohup java -jar gcloud-boot-8.0.jar &
4. 接口测试（以获取vpc列表为例）
curl -i -X POST -H "X-Auth-Token: 4723ad1b-5190-3fc0-8c1b-96581dda3841" http://127.0.0.1:8089/ecs/DescribeVpcs.do

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)