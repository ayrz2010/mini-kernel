package com.gcloud.controller.slb.model;

import java.util.ArrayList;
import java.util.List;

public class DescribeVServerGroupResponse {
	private List<BackendServer> backendServers = new ArrayList<BackendServer>();
	public List<BackendServer> getBackendServers() {
		return backendServers;
	}
	public void setBackendServers(List<BackendServer> backendServers) {
		this.backendServers = backendServers;
	}
}
