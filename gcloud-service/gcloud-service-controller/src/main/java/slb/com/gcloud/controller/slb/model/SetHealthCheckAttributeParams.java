package com.gcloud.controller.slb.model;

public class SetHealthCheckAttributeParams {
	private String resourceId;

	/**
	 * 健康检查开关：on、off
	 */
	private String healthCheck;

	/**
	 * 监听器协议，只适用于阿里，取值：TCP | HTTP | HTTPS
	 */
	private String protocol;

	/**
	 * openstack： 对应maxRetries <br>
	 * 阿里：对所有协议适用，健康检查连续失败多少次后，将后端服务器的健康检查状态由success判定为fail
	 */
	private Integer unhealthyThreshold;

	/**
	 * openstack： 对应delay <br>
	 * 阿里：对所有协议适用，健康检查的时间间隔
	 */
	private Integer healthCheckInterval;

	/**
	 * openstack： 对应urlPath <br>
	 * 阿里：对所有协议适用，用于健康检查的URI
	 */
	private String healthCheckURI;

	/**
	 * openstack： 没有这个属性 <br>
	 * 阿里：对所有协议适用，健康检查连续成功多少次后，将后端服务器的健康检查状态由fail判定为success
	 */
	private Integer healthyThreshold;

	/**
	 * openstack： 对应timeout <br>
	 * 阿里：对http、https适用，接收来自运行状况检查的响应需要等待的时间。如果后端ECS在指定的时间内没有正确响应，则判定为健康检查失败。
	 */
	private Integer healthCheckTimeout;

	/**
	 * openstack： 对应type <br>
	 * 阿里：对tcp适用，健康检查类型,取值：tcp | http
	 */
	private String healthCheckType;
	
	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	public String getHealthCheck() {
		return healthCheck;
	}

	public void setHealthCheck(String healthCheck) {
		this.healthCheck = healthCheck;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public Integer getUnhealthyThreshold() {
		return unhealthyThreshold;
	}

	public void setUnhealthyThreshold(Integer unhealthyThreshold) {
		this.unhealthyThreshold = unhealthyThreshold;
	}

	public Integer getHealthCheckInterval() {
		return healthCheckInterval;
	}

	public void setHealthCheckInterval(Integer healthCheckInterval) {
		this.healthCheckInterval = healthCheckInterval;
	}

	public Integer getHealthCheckTimeout() {
		return healthCheckTimeout;
	}

	public void setHealthCheckTimeout(Integer healthCheckTimeout) {
		this.healthCheckTimeout = healthCheckTimeout;
	}

	public String getHealthCheckType() {
		return healthCheckType;
	}

	public void setHealthCheckType(String healthCheckType) {
		this.healthCheckType = healthCheckType;
	}

	public String getHealthCheckURI() {
		return healthCheckURI;
	}

	public void setHealthCheckURI(String healthCheckURI) {
		this.healthCheckURI = healthCheckURI;
	}

	public Integer getHealthyThreshold() {
		return healthyThreshold;
	}

	public void setHealthyThreshold(Integer healthyThreshold) {
		this.healthyThreshold = healthyThreshold;
	}
}
