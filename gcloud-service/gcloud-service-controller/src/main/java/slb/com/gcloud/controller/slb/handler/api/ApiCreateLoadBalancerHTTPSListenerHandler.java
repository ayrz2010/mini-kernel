package com.gcloud.controller.slb.handler.api;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.controller.slb.service.ILoadBalancerListenerService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.slb.msg.api.ApiCreateLoadBalancerHTTPSListenerMsg;
import com.gcloud.header.slb.msg.api.ApiCreateLoadBalancerHTTPSListenerReplyMsg;

@GcLog(taskExpect = "创建HTTPS监听器")
@ApiHandler(module=Module.SLB,action="CreateLoadBalancerHTTPSListener")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.LOADBALANCER, resourceIdField = "loadBalancerId")
public class ApiCreateLoadBalancerHTTPSListenerHandler extends MessageHandler<ApiCreateLoadBalancerHTTPSListenerMsg, ApiCreateLoadBalancerHTTPSListenerReplyMsg>{

	@Autowired
	ILoadBalancerListenerService service;
	
	@Override
	public ApiCreateLoadBalancerHTTPSListenerReplyMsg handle(ApiCreateLoadBalancerHTTPSListenerMsg msg)
			throws GCloudException {
		// TODO Auto-generated method stub
		ApiCreateLoadBalancerHTTPSListenerReplyMsg reply = new ApiCreateLoadBalancerHTTPSListenerReplyMsg();
		String listenerId = service.createLoadBalancerHTTPSListener(msg.getLoadBalancerId(), msg.getListenerPort(), msg.getvServerGroupId(), msg.getServerCertificateId());
		reply.setListenerId(listenerId);
		return reply;
	}

}
