package com.gcloud.controller.slb.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.gcloud.controller.slb.entity.LoadBalancer;
import com.gcloud.controller.utils.SqlUtil;
import com.gcloud.core.currentUser.policy.enums.UserResourceFilterPolicy;
import com.gcloud.core.currentUser.policy.model.FilterPolicyModel;
import com.gcloud.core.currentUser.policy.service.IUserResourceFilterPolicy;
import com.gcloud.core.service.SpringUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.slb.model.LoadBalancerModel;

@Repository
public class LoadBalancerDao extends JdbcBaseDaoImpl<LoadBalancer, String> {
	
	
	public  void updateLoadBalancerStatus(String loadBalancerId, String status) {
		LoadBalancer lber = this.getById(loadBalancerId);
		lber.setStatus(status);
		this.update(lber);
		
	}
	
	public PageResult<LoadBalancerModel> getByPage(int pageNum, int pageSize, List<String> loadBalancers, CurrentUser currentUser) {
		IUserResourceFilterPolicy filterPolicy = (IUserResourceFilterPolicy)SpringUtil.getBean(UserResourceFilterPolicy.TYPICAL.getFilterPolicyClazz());
		FilterPolicyModel sqlModel = filterPolicy.filterPolicy(currentUser, "t.");
		StringBuffer sql = new StringBuffer();
		List<Object> values = new ArrayList<>();
		
		sql.append("select t.*,GROUP_CONCAT(CONCAT_WS('#',l.id,l.listener_port,l.listener_protocol)) listener,s.network_id vpc_id,s.cidr address from gc_slb t "
				+ " left join gc_slb_listener l on t.id = l.loadbalancer_id"
				+ " left join gc_subnets s on t.vswitch_id = s.id");
		sql.append(" where 1 = 1");
		
		if(loadBalancers != null && loadBalancers.size() > 0){      
		    String inPreSql = SqlUtil.inPreStr(loadBalancers.size());
		    sql.append(" and t.id in (").append(inPreSql).append(")");
		    values.addAll(loadBalancers);
		}
		
		sql.append(sqlModel.getWhereSql());
		values.addAll(sqlModel.getParams());
		
		sql.append(" group by t.id");
		
		sql.append(" order by t.create_time desc");
		
		return this.findBySql(sql.toString(), values, pageNum, pageSize, LoadBalancerModel.class);
	}
	
	public List<LoadBalancerModel> list(List<String> loadBalancers) {
		StringBuffer sql = new StringBuffer();
		List<Object> values = new ArrayList<>();
		
		sql.append("select t.*,GROUP_CONCAT(CONCAT_WS('#',l.id,l.listener_port,l.listener_protocol)) listener,s.network_id vpc_id,s.cidr address from gc_slb t "
				+ " left join gc_slb_listener l on t.id = l.loadbalancer_id"
				+ " left join gc_subnets s on t.vswitch_id = s.id");
		sql.append(" where 1 = 1");
		
		if(loadBalancers != null && loadBalancers.size() > 0){      
		   String inPreSql = SqlUtil.inPreStr(loadBalancers.size());
		   sql.append(" and t.id in (").append(inPreSql).append(")");
		   values.addAll(loadBalancers);
		}
		
		sql.append(" group by t.id");
		
		sql.append(" order by t.create_time desc");
		
		return this.findBySql(sql.toString(), values, LoadBalancerModel.class);
	}

}
