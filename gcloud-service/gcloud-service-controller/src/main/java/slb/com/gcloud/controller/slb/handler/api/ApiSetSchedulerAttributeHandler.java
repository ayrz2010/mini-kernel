package com.gcloud.controller.slb.handler.api;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.slb.service.ISchedulerService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.slb.msg.api.ApiSetSchedulerAttributeMsg;
import com.gcloud.header.slb.msg.api.ApiSetSchedulerAttributeReplyMsg;

@GcLog(taskExpect = "设置调度策略")
@ApiHandler(module=Module.SLB,action="SetSchedulerAttribute")
public class ApiSetSchedulerAttributeHandler extends MessageHandler<ApiSetSchedulerAttributeMsg, ApiSetSchedulerAttributeReplyMsg>{

	@Autowired
	ISchedulerService service;
	
	@Override
	public ApiSetSchedulerAttributeReplyMsg handle(ApiSetSchedulerAttributeMsg msg) throws GCloudException {
		// TODO Auto-generated method stub
		service.setSchedulerAttribute(msg.getResourceId(), msg.getProtocol(), msg.getScheduler());
		ApiSetSchedulerAttributeReplyMsg reply = new ApiSetSchedulerAttributeReplyMsg();
		return reply;
	}

}
