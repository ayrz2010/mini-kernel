package com.gcloud.controller.slb.handler.api.vservergroup;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.slb.model.BackendServer;
import com.gcloud.controller.slb.model.DescribeVServerGroupAttributeResponse;
import com.gcloud.controller.slb.service.IVServerGroupService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.slb.model.BackendServerResponse;
import com.gcloud.header.slb.model.BackendServerSetType;
import com.gcloud.header.slb.msg.api.ApiDescribeVServerGroupAttributeMsg;
import com.gcloud.header.slb.msg.api.ApiDescribeVServerGroupAttributeReplyMsg;

@ApiHandler(module=Module.SLB,action="DescribeVServerGroupAttribute")
public class ApiDescribeVServerGroupAttributeHandler extends MessageHandler<ApiDescribeVServerGroupAttributeMsg, ApiDescribeVServerGroupAttributeReplyMsg> {
	@Autowired
	private IVServerGroupService vServerGroupService;
	@Override
	public ApiDescribeVServerGroupAttributeReplyMsg handle(ApiDescribeVServerGroupAttributeMsg msg)
			throws GCloudException {
		// TODO Auto-generated method stub
		DescribeVServerGroupAttributeResponse response= vServerGroupService.describeVServerGroupAttribute(msg.getvServerGroupId());
		ApiDescribeVServerGroupAttributeReplyMsg reply=new ApiDescribeVServerGroupAttributeReplyMsg();
		reply.setVServerGroupId(response.getvServerGroupId());
		reply.setVServerGroupName(response.getvServerGroupName());
		BackendServerResponse backendServerResponse=new BackendServerResponse();
		for(BackendServer backendServer:response.getBackendServers()) {
			BackendServerSetType type=new BackendServerSetType();
			type.setMemberId(backendServer.getMemberId());
			type.setPort(backendServer.getPort());
			type.setServerId(backendServer.getServerId());
			type.setWeight(backendServer.getWeight());
			type.setType(backendServer.getType());
			backendServerResponse.getBackendServer().add(type);
		}
		reply.setBackendServers(backendServerResponse);
		return reply;
	}

}
