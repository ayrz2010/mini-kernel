package com.gcloud.controller.slb.async;

import org.openstack4j.model.network.ext.LbProvisioningStatus;
import org.openstack4j.model.network.ext.LoadBalancerV2;

import com.gcloud.controller.async.LogAsync;
import com.gcloud.controller.provider.NeutronProviderProxy;
import com.gcloud.controller.slb.dao.LoadBalancerDao;
import com.gcloud.core.async.AsyncResult;
import com.gcloud.core.async.AsyncStatus;
import com.gcloud.core.service.SpringUtil;

public class CheckNeutronCreateloadBalancerAsync extends LogAsync {
	
	private String  loadBalancerId;
    private String status;
	public String getLoadBalancerId() {
		return loadBalancerId;
	}

	public void setLoadBalancerId(String loadBalancerId) {
		this.loadBalancerId = loadBalancerId;
	}

	@Override
	public long timeout() {
		// TODO Auto-generated method stub
		
		return 0;
	}

	@Override
	public AsyncResult execute() {
		// TODO Auto-generated method stub
		AsyncStatus asyncStatus = null;
		NeutronProviderProxy proxy = SpringUtil.getBean(NeutronProviderProxy.class);
		LoadBalancerV2  lber = proxy.getLoadBalancer(getLoadBalancerId());
		if(lber.getProvisioningStatus().equals(LbProvisioningStatus.ACTIVE)) {
			asyncStatus = AsyncStatus.SUCCEED;
			this.status = lber.getProvisioningStatus().name();
		}else if(lber.getProvisioningStatus().equals(LbProvisioningStatus.ERROR) || lber.getProvisioningStatus().equals(LbProvisioningStatus.INACTIVE)) {
			asyncStatus = AsyncStatus.FAILED;
			this.status = lber.getProvisioningStatus().name();
		}else {
			asyncStatus =  AsyncStatus.RUNNING;
			this.status = lber.getProvisioningStatus().name();
		}
		return new AsyncResult(asyncStatus);
	}
	
	private void handler() {
		LoadBalancerDao  dao = SpringUtil.getBean(LoadBalancerDao.class);
		dao.updateLoadBalancerStatus(getLoadBalancerId(), status);
		
	}
    public void successHandler(){
        this.handler();
    }
    public void failHandler(){
    	this.handler();
    }

}
