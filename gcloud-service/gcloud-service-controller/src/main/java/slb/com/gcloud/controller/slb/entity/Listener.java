package com.gcloud.controller.slb.entity;

import java.util.Date;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

@Table(name="gc_slb_listener",jdbc="controllerJdbcTemplate")
public class Listener {
	
	@ID
	private String id;
	private Integer listenerPort;
	private String listenerProtocol;
	private String status;
	private String loadbalancerId;
	private String serverCertificateId;
	private String vserverGroupId;
	private String vserverGroupName;
	private Integer provider;
	private String providerRefId;
	private Date updatedAt;
	
	public static final String ID = "id";
	public static final String LISTENER_PORT = "listenerPort";
	public static final String LISTENER_PROTOCOL = "listenerProtocol";
	public static final String STATUS = "status";
	public static final String LOADBALANCER_ID = "loadbalancerId";
	public static final String SERVER_CERTIFICATE_ID = "serverCertificateId";
	public static final String VSERVER_GROUP_ID = "vserverGroupId";
	public static final String VSERVER_GROUP_NAME = "vserverGroupName";
	public static final String PROVIDER = "provider";
	public static final String PROVIDER_REF_ID = "providerRefId";
	public static final String UPDATED_AT = "updatedAt";
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Integer getListenerPort() {
		return listenerPort;
	}
	public void setListenerPort(Integer listenerPort) {
		this.listenerPort = listenerPort;
	}
	public String getListenerProtocol() {
		return listenerProtocol;
	}
	public void setListenerProtocol(String listenerProtocol) {
		this.listenerProtocol = listenerProtocol;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getLoadbalancerId() {
		return loadbalancerId;
	}
	public void setLoadbalancerId(String loadbalancerId) {
		this.loadbalancerId = loadbalancerId;
	}
	public String getServerCertificateId() {
		return serverCertificateId;
	}
	public void setServerCertificateId(String serverCertificateId) {
		this.serverCertificateId = serverCertificateId;
	}
	public String getVserverGroupId() {
		return vserverGroupId;
	}
	public void setVserverGroupId(String vserverGroupId) {
		this.vserverGroupId = vserverGroupId;
	}
	public String getVserverGroupName() {
		return vserverGroupName;
	}
	public void setVserverGroupName(String vserverGroupName) {
		this.vserverGroupName = vserverGroupName;
	}
	public Integer getProvider() {
		return provider;
	}
	public void setProvider(Integer provider) {
		this.provider = provider;
	}
	public String getProviderRefId() {
		return providerRefId;
	}
	public void setProviderRefId(String providerRefId) {
		this.providerRefId = providerRefId;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	public String updateId(String id) {
        this.setId(id);
        return ID;
    }
	public String updateListenerPort(Integer listenerPort) {
        this.setListenerPort(listenerPort);
        return LISTENER_PORT;
    }
	public String updateListenerProtocol(String listenerProtocol) {
        this.setListenerProtocol(listenerProtocol);
        return LISTENER_PROTOCOL;
    }
	public String updateStatus(String status) {
        this.setStatus(status);
        return STATUS;
    }
	public String updateLoadbalancerId(String loadbalancerId) {
        this.setLoadbalancerId(loadbalancerId);
        return LOADBALANCER_ID;
    }
	public String updateServerCertificateId(String serverCertificateId) {
        this.setServerCertificateId(serverCertificateId);
        return SERVER_CERTIFICATE_ID;
    }
	public String updateVserverGroupId(String vserverGroupId) {
        this.setVserverGroupId(vserverGroupId);
        return VSERVER_GROUP_ID;
    }
	public String updateVserverGroupName(String vserverGroupName) {
        this.setVserverGroupName(vserverGroupName);
        return VSERVER_GROUP_NAME;
    }
	public String updateProvider(Integer provider) {
        this.setProvider(provider);
        return PROVIDER;
    }
	public String updateProviderRefId(String providerRefId) {
        this.setProviderRefId(providerRefId);
        return PROVIDER_REF_ID;
    }
	public String updateUpdatedAt(Date updatedAt) {
        this.setUpdatedAt(updatedAt);
        return UPDATED_AT;
    }
	
	
}
