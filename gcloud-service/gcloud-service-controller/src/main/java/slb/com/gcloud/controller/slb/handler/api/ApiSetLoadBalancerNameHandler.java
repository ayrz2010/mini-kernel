package com.gcloud.controller.slb.handler.api;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.controller.slb.service.ILoadBalancerService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.slb.msg.api.SetLoadBalancerNameMsg;
import com.gcloud.header.slb.msg.api.SetLoadBalancerNameReplyMsg;


@ApiHandler(module=Module.SLB,action="SetLoadBalancerName")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.LOADBALANCER, resourceIdField = "loadBalancerId")
public class ApiSetLoadBalancerNameHandler extends MessageHandler<SetLoadBalancerNameMsg, SetLoadBalancerNameReplyMsg> {

	
	@Autowired
	ILoadBalancerService   service;
	@Override
	public SetLoadBalancerNameReplyMsg handle(SetLoadBalancerNameMsg msg) throws GCloudException {
		
		// TODO Auto-generated method stub
		service.setLoadBalancerName(msg.getLoadBalancerId(), msg.getLoadBalancerName());
		return new  SetLoadBalancerNameReplyMsg();
	}

}
