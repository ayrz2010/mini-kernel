
package com.gcloud.controller.slb.provider;

import com.gcloud.controller.IResourceProvider;
import com.gcloud.controller.slb.model.DescribeLoadBalancerAttributeResponse;
import com.gcloud.controller.slb.model.DescribeLoadBalancersParams;
import com.gcloud.controller.slb.entity.LoadBalancer;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.slb.msg.api.CreateLoadBalancerReplyMsg;

import java.util.List;
import java.util.Map;

public interface ILoadBalancerProvider extends IResourceProvider {

    CreateLoadBalancerReplyMsg createLb(String loadBalancerName, String vSwitchId, String regionId, CurrentUser currentUser);

    void updateBalancerName(String loadBalancerId, String loadBalancerName);

    void deleteLoadBalancer(String loadBalancerId);
    
    DescribeLoadBalancerAttributeResponse describeLoadBalancerAttribute(String loadBalancerId);
    
    DescribeLoadBalancersParams describeLoadBalancers(List<String> loadBalancers);

    List<LoadBalancer> list(Map<String, String> filters);
}
