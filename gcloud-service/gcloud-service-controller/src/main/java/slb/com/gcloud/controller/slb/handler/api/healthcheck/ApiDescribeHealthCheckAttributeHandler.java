package com.gcloud.controller.slb.handler.api.healthcheck;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.slb.model.DescribeHealthCheckAttributeResponse;
import com.gcloud.controller.slb.service.IHealthCheckService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.slb.msg.api.ApiDescribeHealthCheckAttributeMsg;
import com.gcloud.header.slb.msg.api.ApiDescribeHealthCheckAttributeReplyMsg;
@ApiHandler(module=Module.SLB,action="DescribeHealthCheckAttribute")
public class ApiDescribeHealthCheckAttributeHandler extends MessageHandler<ApiDescribeHealthCheckAttributeMsg,ApiDescribeHealthCheckAttributeReplyMsg> {
	@Autowired
	private IHealthCheckService healthCheckService; 
	@Override
	public ApiDescribeHealthCheckAttributeReplyMsg handle(ApiDescribeHealthCheckAttributeMsg msg)
			throws GCloudException {
		// TODO Auto-generated method stub
		DescribeHealthCheckAttributeResponse response= healthCheckService.describeHealthCheckAttribute(msg.getResourceId());
		ApiDescribeHealthCheckAttributeReplyMsg reply=new ApiDescribeHealthCheckAttributeReplyMsg();
		reply.setHealthCheck(response.getHealthCheck());
		reply.setHealthCheckInterval(response.getHealthCheckInterval());
		reply.setHealthCheckTimeout(response.getHealthCheckTimeout());
		reply.setHealthCheckType(response.getHealthCheckType());
		reply.setHealthCheckURI(response.getHealthCheckURI());
		reply.setHealthyThreshold(response.getHealthyThreshold());
		reply.setUnhealthyThreshold(response.getUnhealthyThreshold());
		return reply;
	}

}
