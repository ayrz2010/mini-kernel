package com.gcloud.controller.slb.handler.api;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.controller.slb.service.ILoadBalancerListenerService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.slb.msg.api.ApiCreateLoadBalancerHTTPListenerMsg;
import com.gcloud.header.slb.msg.api.ApiCreateLoadBalancerHTTPListenerReplyMsg;

@GcLog(taskExpect = "创建HTTP监听器")
@ApiHandler(module=Module.SLB,action="CreateLoadBalancerHTTPListener")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.LOADBALANCER, resourceIdField = "loadBalancerId")
public class ApiCreateLoadBalancerHTTPListenerHandler extends MessageHandler<ApiCreateLoadBalancerHTTPListenerMsg, ApiCreateLoadBalancerHTTPListenerReplyMsg> {

	@Autowired
	ILoadBalancerListenerService service;

	@Override
	public ApiCreateLoadBalancerHTTPListenerReplyMsg handle(ApiCreateLoadBalancerHTTPListenerMsg msg) throws GCloudException {
		// TODO Auto-generated method stub
		ApiCreateLoadBalancerHTTPListenerReplyMsg reply = new ApiCreateLoadBalancerHTTPListenerReplyMsg();
		String listenerId = service.createLoadBalancerHTTPListener(msg.getLoadBalancerId(), msg.getListenerPort(), msg.getvServerGroupId());
		reply.setListenerId(listenerId);
		return reply;
	}

	
}
