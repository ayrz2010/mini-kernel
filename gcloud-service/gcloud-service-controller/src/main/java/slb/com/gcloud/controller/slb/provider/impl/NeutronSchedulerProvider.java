package com.gcloud.controller.slb.provider.impl;

import org.openstack4j.model.network.ext.LbPoolV2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gcloud.controller.provider.NeutronProviderProxy;
import com.gcloud.controller.slb.model.DescribeSchedulerAttributeResponse;
import com.gcloud.controller.slb.provider.ISchedulerProvider;
import com.gcloud.header.enums.ProviderType;
import com.gcloud.header.enums.ResourceType;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class NeutronSchedulerProvider implements ISchedulerProvider {

	@Autowired
    private NeutronProviderProxy neutronProviderProxy;

    public ResourceType resourceType() {
        return ResourceType.SCHEDULER;
    }

    public ProviderType providerType() {
        return ProviderType.NEUTRON;
    }
	
	@Override
	public void setSchedulerAttribute(String resourceId, String protocol, String scheduler) {
		// TODO Auto-generated method stub
		neutronProviderProxy.setSchedulerAttribute(resourceId, protocol, scheduler);
	}

	@Override
	public DescribeSchedulerAttributeResponse describeSchedulerAttribute(String resourceId, String protocol) {
		// TODO Auto-generated method stub
		LbPoolV2 lbPoolV2 = neutronProviderProxy.describeSchedulerAttribute(resourceId, protocol);
		
		DescribeSchedulerAttributeResponse response = new DescribeSchedulerAttributeResponse();
		response.setScheduler(lbPoolV2.getName());
		
		return response;
	}
}
