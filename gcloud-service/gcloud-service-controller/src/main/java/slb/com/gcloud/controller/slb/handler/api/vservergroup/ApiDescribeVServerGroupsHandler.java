package com.gcloud.controller.slb.handler.api.vservergroup;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.slb.entity.VServerGroup;
import com.gcloud.controller.slb.service.IVServerGroupService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.slb.model.VServerGroupSetType;
import com.gcloud.header.slb.msg.api.ApiDescribeVServerGroupsMsg;
import com.gcloud.header.slb.msg.api.ApiDescribeVServerGroupsReplyMsg;

@ApiHandler(module=Module.SLB,action="DescribeVServerGroups")
public class ApiDescribeVServerGroupsHandler extends MessageHandler<ApiDescribeVServerGroupsMsg, ApiDescribeVServerGroupsReplyMsg> {
	@Autowired
	IVServerGroupService vServerGroupService;
	
	@Override
	public ApiDescribeVServerGroupsReplyMsg handle(ApiDescribeVServerGroupsMsg msg) throws GCloudException {
		// TODO Auto-generated method stub
		List<VServerGroup> vServceGroups= vServerGroupService.describeVServerGroups(msg.getLoadBalancerId());
		ApiDescribeVServerGroupsReplyMsg reply=new ApiDescribeVServerGroupsReplyMsg();
		for(VServerGroup vServerGroup:vServceGroups) {
			VServerGroupSetType type=new VServerGroupSetType();
			type.setvServerGroupId(vServerGroup.getId());
			type.setvServerGroupName(vServerGroup.getName());
			type.setvServerGroupProtocol(vServerGroup.getProtocol());
			reply.getVServerGroups().add(type);
		}
		return reply;
	}

}
