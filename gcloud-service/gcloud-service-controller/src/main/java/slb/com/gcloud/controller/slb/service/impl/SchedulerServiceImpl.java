package com.gcloud.controller.slb.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gcloud.controller.ResourceProviders;
import com.gcloud.controller.slb.dao.ListenerDao;
import com.gcloud.controller.slb.dao.LoadBalancerDao;
import com.gcloud.controller.slb.dao.SchedulerDao;
import com.gcloud.controller.slb.dao.VServerGroupDao;
import com.gcloud.controller.slb.entity.LoadBalancer;
import com.gcloud.controller.slb.entity.Scheduler;
import com.gcloud.controller.slb.entity.VServerGroup;
import com.gcloud.controller.slb.model.DescribeSchedulerAttributeResponse;
import com.gcloud.controller.slb.provider.ILoadBalancerListenerProvider;
import com.gcloud.controller.slb.provider.ISchedulerProvider;
import com.gcloud.controller.slb.service.ISchedulerService;
import com.gcloud.core.condition.ConditionalSlb;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.header.enums.ResourceType;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Transactional(propagation = Propagation.REQUIRES_NEW)
@Service
public class SchedulerServiceImpl implements ISchedulerService {

	@Autowired
    private ListenerDao listenerDao;
	
	@Autowired
	private VServerGroupDao vServerGroupDao;
	
	@Autowired
	private SchedulerDao schedulerDao;
	
	@Override
	public void setSchedulerAttribute(String resourceId, String protocol, String scheduler) {
		// TODO Auto-generated method stub
		VServerGroup vServerGroup = vServerGroupDao.getById(resourceId);
		if (vServerGroup == null) {
            log.debug("找不到服务器组");
            throw new GCloudException("0130103::找不到服务器组");
        }
		Scheduler entity = schedulerDao.findUniqueByProperty(Scheduler.VSERVER_GROUP_ID, resourceId);
		try {
			if (null == entity) {
				entity = new Scheduler();
				String uuid = UUID.randomUUID().toString();
				entity.setId(uuid);
				entity.setProtocol(protocol);
				entity.setProvider(vServerGroup.getProvider());
				entity.setProviderRefId(uuid);
				entity.setVserverGroupId(vServerGroup.getId());
				entity.setScheduler(scheduler);
				schedulerDao.save(entity);
	        }else {
	        	List<String> updateField = new ArrayList<String>();
	        	updateField.add(entity.updateScheduler(scheduler));
	        	updateField.add(entity.updateUpdatedAt(new Date()));
	        	schedulerDao.update(entity, updateField);
	        }
			ISchedulerProvider provider = this.getProvider(entity.getProvider());
			provider.setSchedulerAttribute(vServerGroup.getProviderRefId(), protocol, scheduler);
		} catch(Exception e) {
			log.debug("设置负载均衡调度策略失败");
            throw new GCloudException("0130104::设置负载均衡调度策略失败");
		}	
		
	}

	@Override
	public DescribeSchedulerAttributeResponse describeSchedulerAttribute(String resourceId, String protocol) {
		// TODO Auto-generated method stub
		DescribeSchedulerAttributeResponse response = new DescribeSchedulerAttributeResponse();
		Scheduler entity = schedulerDao.findUniqueByProperty(Scheduler.VSERVER_GROUP_ID, resourceId);
//		if (null == entity) {
//			log.debug("找不到调度策略");
//            throw new GCloudException("0130202::找不到调度策略");
//        }
		if (entity != null) {
			response.setScheduler(entity.getScheduler());
		}
		return response;
	}

	private ISchedulerProvider getProvider(int providerType) {
		ISchedulerProvider provider = ResourceProviders.checkAndGet(ResourceType.SCHEDULER, providerType);
        return provider;
    }
}
