package com.gcloud.controller.slb.provider.impl;

import java.util.List;

import org.openstack4j.api.Builders;
import org.openstack4j.model.network.ext.HealthMonitorType;
import org.openstack4j.model.network.ext.HealthMonitorV2;
import org.openstack4j.openstack.networking.domain.ext.ListItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.provider.NeutronProviderProxy;
import com.gcloud.controller.slb.model.DescribeHealthCheckAttributeResponse;
import com.gcloud.controller.slb.model.SetHealthCheckAttributeParams;
import com.gcloud.controller.slb.provider.IHealthCheckProvider;

import lombok.extern.slf4j.Slf4j;
@Slf4j
@Component
public class NuetronHealthCheckProviderImpl implements IHealthCheckProvider{
	@Autowired
	private NeutronProviderProxy neutronProviderProxy;
	public void setHealthCheckAttribute(SetHealthCheckAttributeParams params) {
		Boolean healthCheckIsOn = StringUtils.equalsIgnoreCase(params.getHealthCheck(), "on");
		
		List<? extends HealthMonitorV2> healthmonitors = neutronProviderProxy.getClient().networking().lbaasV2().healthMonitor().list();
		if (healthmonitors != null && healthmonitors.size() > 0) {
			for (HealthMonitorV2 healthMonitorV2 : healthmonitors) {
				List<ListItem> temps = healthMonitorV2.getPools();
				if (temps != null && temps.size() > 0) {
					for (ListItem listItem : temps) {
						if (StringUtils.equals(listItem.getId(), params.getResourceId())) {
							if (healthCheckIsOn) {
								neutronProviderProxy.getClient().networking().lbaasV2().healthMonitor()
									.update(healthMonitorV2.getId(), Builders.healthMonitorV2Update()
											.delay(params.getHealthCheckInterval())
											.maxRetries(params.getUnhealthyThreshold())
											.urlPath(params.getHealthCheckURI())
											.timeout(params.getHealthCheckTimeout())
					        				.adminStateUp(true)
											.build());
							}
							else {
								neutronProviderProxy.getClient().networking().lbaasV2().healthMonitor().delete(healthMonitorV2.getId());
							}
							
							log.debug("SetHealthCheckAttribute end...");
						}
					}
				}
			}
		}
		
		if (healthCheckIsOn) {
			neutronProviderProxy.getClient().networking().lbaasV2().healthMonitor()
				.create(Builders.healthmonitorV2()
						.delay(params.getHealthCheckInterval())
						.maxRetries(params.getUnhealthyThreshold())
						.poolId(params.getResourceId())
						.timeout(params.getHealthCheckTimeout())
						.type(HealthMonitorType.forValue(params.getHealthCheckType()))
						.urlPath(params.getHealthCheckURI())
						.adminStateUp(true)
						.build());
		}
	}
	
	public DescribeHealthCheckAttributeResponse describeHealthCheckAttribute(String resourceId) {
		DescribeHealthCheckAttributeResponse response=new DescribeHealthCheckAttributeResponse();
		response.setHealthCheck("off");
		List<? extends HealthMonitorV2> healthmonitors = neutronProviderProxy.getClient().networking().lbaasV2().healthMonitor().list();
		if (healthmonitors != null && healthmonitors.size() > 0) {
			for (HealthMonitorV2 healthMonitorV2 : healthmonitors) {
				List<ListItem> temps = healthMonitorV2.getPools();
				if (temps != null && temps.size() > 0) {
					for (ListItem listItem : temps) {
						if (StringUtils.equals(listItem.getId(), resourceId)) {
							response.setHealthCheck("on");
							response.setHealthCheckInterval(healthMonitorV2.getDelay());
							response.setHealthCheckTimeout(healthMonitorV2.getTimeout());
							response.setHealthCheckType(healthMonitorV2.getType().name());
							response.setHealthCheckURI(healthMonitorV2.getUrlPath());
							response.setUnhealthyThreshold(healthMonitorV2.getMaxRetries());
							
							log.debug("DescribeHealthCheckAttribute end...");
							return response;
						}
					}
				}
			}
		}
		return response;
	}
}
