package com.gcloud.controller.slb.handler.api;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.controller.slb.service.ILoadBalancerListenerService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.slb.msg.api.ApiSetLoadBalancerTCPListenerMsg;
import com.gcloud.header.slb.msg.api.ApiSetLoadBalancerTCPListenerReplyMsg;

@GcLog(taskExpect = "配置TCP监听器")
@ApiHandler(module=Module.SLB,action="SetLoadBalancerTCPListenerAttribute")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.LISTENER, resourceIdField = "listenerId")
public class ApiSetLoadBalancerTCPListenerHandler extends MessageHandler<ApiSetLoadBalancerTCPListenerMsg, ApiSetLoadBalancerTCPListenerReplyMsg>{

	@Autowired
	ILoadBalancerListenerService service;
	
	@Override
	public ApiSetLoadBalancerTCPListenerReplyMsg handle(ApiSetLoadBalancerTCPListenerMsg msg) throws GCloudException {
		// TODO Auto-generated method stub
		service.setLoadBalancerTCPListenerAttribute(msg.getListenerId(), msg.getvServerGroupId());
		ApiSetLoadBalancerTCPListenerReplyMsg reply = new ApiSetLoadBalancerTCPListenerReplyMsg();
		return reply;
	}

}
