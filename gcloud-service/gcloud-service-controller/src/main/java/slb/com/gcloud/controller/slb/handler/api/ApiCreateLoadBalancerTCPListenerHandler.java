package com.gcloud.controller.slb.handler.api;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.controller.slb.service.ILoadBalancerListenerService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.slb.msg.api.ApiCreateLoadBalancerTCPListenerMsg;
import com.gcloud.header.slb.msg.api.ApiCreateLoadBalancerTCPListenerReplyMsg;

@GcLog(taskExpect = "创建TCP监听器")
@ApiHandler(module=Module.SLB,action="CreateLoadBalancerTCPListener")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.LOADBALANCER, resourceIdField = "loadBalancerId")
public class ApiCreateLoadBalancerTCPListenerHandler extends MessageHandler<ApiCreateLoadBalancerTCPListenerMsg, ApiCreateLoadBalancerTCPListenerReplyMsg> {

	@Autowired
	ILoadBalancerListenerService service;

	@Override
	public ApiCreateLoadBalancerTCPListenerReplyMsg handle(ApiCreateLoadBalancerTCPListenerMsg msg)
			throws GCloudException {
		// TODO Auto-generated method stub
		ApiCreateLoadBalancerTCPListenerReplyMsg reply = new ApiCreateLoadBalancerTCPListenerReplyMsg();
		String listenerId = service.createLoadBalancerTCPListener(msg.getLoadBalancerId(), msg.getListenerPort(), msg.getvServerGroupId());
		reply.setListenerId(listenerId);
		return reply;
	}
	
	
}
