package com.gcloud.controller.slb.dao;

import org.springframework.stereotype.Repository;

import com.gcloud.controller.slb.entity.Scheduler;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;

@Repository
public class SchedulerDao extends JdbcBaseDaoImpl<Scheduler, String>{

}
