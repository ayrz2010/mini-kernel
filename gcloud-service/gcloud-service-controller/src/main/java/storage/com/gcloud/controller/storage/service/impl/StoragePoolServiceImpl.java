
package com.gcloud.controller.storage.service.impl;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.ResourceProviders;
import com.gcloud.controller.storage.dao.DiskCategoryDao;
import com.gcloud.controller.storage.dao.StoragePoolDao;
import com.gcloud.controller.storage.entity.DiskCategory;
import com.gcloud.controller.storage.entity.StoragePool;
import com.gcloud.controller.storage.provider.IStoragePoolProvider;
import com.gcloud.controller.storage.service.IStoragePoolService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.compute.enums.StorageType;
import com.gcloud.header.enums.ResourceType;
import com.gcloud.header.storage.StorageErrorCodes;
import com.gcloud.header.storage.model.DiskCategoryModel;
import com.gcloud.header.storage.model.StoragePoolModel;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Transactional(propagation = Propagation.REQUIRED)
@Component
public class StoragePoolServiceImpl implements IStoragePoolService {

    @Autowired
    private StoragePoolDao poolDao;

    @Autowired
    private DiskCategoryDao categoryDao;

    @Override
    public List<DiskCategoryModel> describeDiskCategories(String zoneId) {
        Map<String, Object> props = new HashMap<>();
        if (zoneId != null) {
            props.put("zoneId", zoneId);
        }
        return this.categoryDao.findByProperties(props, DiskCategoryModel.class);
    }

    @Override
    public String createDiskCategory(String zoneId, String name, Integer minSize, Integer maxSize) {
        synchronized (StoragePoolServiceImpl.class) {
            DiskCategory entity = this.categoryDao.findUniqueByProperty("name", name);
            if (entity == null) {
                entity = new DiskCategory();
                entity.setId(StringUtils.genUuid());
                entity.setZoneId(zoneId);
                entity.setName(name);
                entity.setMinSize(minSize);
                entity.setMaxSize(maxSize);
                this.categoryDao.save(entity);
            }
            return entity.getId();
        }
    }

    @Override
    public PageResult<StoragePoolModel> describeStoragePools(int pageNumber, int pageSize) {
        return this.poolDao.describeStoragePools(pageNumber, pageSize, StoragePoolModel.class);
    }

    private DiskCategory checkAndGetCategory(String categoryId) {
        DiskCategory res = this.categoryDao.getById(categoryId);
        if (res == null) {
            throw new GCloudException(StorageErrorCodes.NO_SUCH_STORAGE_TYPE);
        }
        return res;
    }

    @Override
    public String createStoragePool(String displayName, Integer providerType, String storageType, String poolName, String zoneId, String categoryId) throws GCloudException {
        if (StorageType.value(storageType) == null) {
            throw new GCloudException(StorageErrorCodes.NO_SUCH_STORAGE_TYPE);
        }
        this.checkAndGetCategory(categoryId);
        StoragePool pool = new StoragePool();
        pool.setId(StringUtils.genUuid());
        synchronized (StringUtils.syncStringObject("" + providerType + storageType + poolName)) {
            Map<String, Object> props = new HashMap<>();
            props.put(StoragePool.PROVIDER, providerType);
            props.put(StoragePool.STORAGE_TYPE, storageType);
            props.put(StoragePool.POOL_NAME, poolName);
            if (this.poolDao.findByProperties(props).size() > 0) {
                throw new GCloudException(StorageErrorCodes.POOL_ALREADY_EXISTS);
            }
            String refId = this.getProvider(providerType).createStoragePool(pool.getId(), storageType, poolName);
            {
                pool.setDisplayName(displayName);
                pool.setProvider(providerType);
                pool.setStorageType(storageType);
                pool.setPoolName(poolName);
                pool.setProvider(providerType);
                pool.setProviderRefId(refId);
                pool.setZoneId(zoneId);
                pool.setCategoryId(categoryId);
                this.poolDao.save(pool);
            }
        }
        return pool.getId();
    }

    @Override
    public void modifyStoragePool(String poolId, String displayName) throws GCloudException {
        synchronized (StringUtils.syncStringObject(poolId)) {
            StoragePool pool = this.poolDao.checkAndGet(poolId);
            if (!StringUtils.equals(pool.getDisplayName(), displayName)) {
                List<String> updateFields = new ArrayList<>();
                updateFields.add(pool.updateDisplayName(displayName));
                this.poolDao.update(pool, updateFields);
            }
        }
    }

    @Override
    public void deleteStoragePool(String poolId) throws GCloudException {
        synchronized (StringUtils.syncStringObject(poolId)) {
            StoragePool pool = this.poolDao.checkAndGet(poolId);
            this.getProvider(pool.getProvider()).deleteStoragePool(pool.getStorageType(), pool.getProviderRefId(), pool.getPoolName());
            this.poolDao.deleteById(poolId);
        }
    }

    private IStoragePoolProvider getProvider(int providerType) {
        IStoragePoolProvider provider = ResourceProviders.checkAndGet(ResourceType.STORAGE_POOL, providerType);
        return provider;
    }

}
