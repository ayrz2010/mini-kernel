package com.gcloud.controller.storage.service;

import com.gcloud.controller.storage.model.DescribeSnapshotsParams;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.storage.model.SnapshotType;

/**
 * Created by yaowj on 2018/11/7.
 */
public interface ISnapshotService {

    String createSnapshot(String diskId, String name, String description, CurrentUser currentUser, String taskId) throws GCloudException;

    void updateSnapshot(String id, String name, String description) throws GCloudException;

    void deleteSnapshot(String id, String taskId) throws GCloudException;

    void resetSnapshot(String snapshotId, String diskId) throws GCloudException;

    PageResult<SnapshotType> describeSnapshots(DescribeSnapshotsParams params, CurrentUser currentUser);

}
