
package com.gcloud.controller.storage.dao;

import com.gcloud.controller.storage.entity.StoragePool;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.framework.db.PageResult;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.header.storage.StorageErrorCodes;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class StoragePoolDao extends JdbcBaseDaoImpl<StoragePool, String> {

    public StoragePool checkAndGet(String zoneId, String categoryId) throws GCloudException {
        Map<String, Object> props = new HashMap<>();
        props.put(StoragePool.ZONE_ID, zoneId);
        props.put(StoragePool.CATEGORY_ID, categoryId);
        List<StoragePool> res = this.findByProperties(props);
        if (res.isEmpty()) {
            throw new GCloudException(StorageErrorCodes.FAILED_TO_FIND_POOL);
        }
        return res.get(0);
    }

    public StoragePool checkAndGet(String poolId) throws GCloudException {
        StoragePool pool = this.getById(poolId);
        if (pool == null) {
            throw new GCloudException(StorageErrorCodes.FAILED_TO_FIND_POOL);
        }
        return pool;
    }

    public <E> PageResult<E> describeStoragePools(int pageNumber, int pageSize, Class<E> clazz) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT * FROM gc_storage_pools");
        sql.append(" ORDER BY pool_name ASC");
        return this.findBySql(sql.toString(), new ArrayList<>(), pageNumber, pageSize, clazz);
    }

}
