package com.gcloud.controller.storage.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.ResourceProviders;
import com.gcloud.controller.storage.dao.SnapshotDao;
import com.gcloud.controller.storage.dao.VolumeDao;
import com.gcloud.controller.storage.entity.Snapshot;
import com.gcloud.controller.storage.entity.Volume;
import com.gcloud.controller.storage.model.DescribeSnapshotsParams;
import com.gcloud.controller.storage.provider.ISnapshotProvider;
import com.gcloud.controller.storage.service.ISnapshotService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.enums.ResourceType;
import com.gcloud.header.storage.enums.VolumeStatus;
import com.gcloud.header.storage.model.SnapshotType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yaowj on 2018/12/7.
 */
@Component
@Transactional(propagation = Propagation.REQUIRED)
public class SnapshotServiceImpl implements ISnapshotService {

    @Autowired
    private VolumeDao volumeDao;

    @Autowired
    private SnapshotDao snapshotDao;

    @Override
    public PageResult<SnapshotType> describeSnapshots(DescribeSnapshotsParams params, CurrentUser currentUser) {

        if (params == null) {
            params = new DescribeSnapshotsParams();
        }

        String snapshotJsonIds = params.getSnapshotIds();
        if (snapshotJsonIds != null) {
            List<String> idList = JSONObject.parseArray(snapshotJsonIds, String.class);
            if (idList == null) {
                throw new GCloudException("0070401::快照ID列表格式不正确");
            }
            if (idList.size() > 100) {
                throw new GCloudException("0070402::最多过滤100个快照ID");
            }
            params.setSnapshotIdList(idList);
        }

        return snapshotDao.describeSnapshots(params, SnapshotType.class, currentUser);
    }

    @Override
    public String createSnapshot(String diskId, String name, String description, CurrentUser currentUser, String taskId) throws GCloudException {
        Volume volume = volumeDao.getById(diskId);
        if (volume == null) {
            throw new GCloudException("0070102::找不到对应的磁盘");
        }
        String snapshotId = StringUtils.genUuid();
        this.getProvider(volume.getProvider()).createSnapshot(volume, snapshotId, name, description, currentUser, taskId);
        return snapshotId;
    }

    @Override
    public void updateSnapshot(String id, String name, String description) throws GCloudException {
        Snapshot snapshot = snapshotDao.getById(id);
        if (snapshot == null) {
            throw new GCloudException("0070202::找不到对应的快照");
        }

        List<String> updateFields = new ArrayList<>();
        Snapshot updateSnap = new Snapshot();
        updateSnap.setId(id);
        updateFields.add(updateSnap.updateDisplayName(name));

        snapshotDao.update(updateSnap, updateFields);
        this.getProvider(snapshot.getProvider()).updateSnapshot(snapshot.getProviderRefId(), name, description);
    }

    @Override
    public void deleteSnapshot(String id, String taskId) throws GCloudException {
        Snapshot snapshot = snapshotDao.getById(id);
        if (snapshot == null) {
            throw new GCloudException("0070302::找不到对应的快照");
        }
        Volume volume = volumeDao.getById(snapshot.getVolumeId());
        snapshotDao.updateStatus(id, VolumeStatus.DELETING.value());
        this.getProvider(snapshot.getProvider()).deleteSnapshot(volume, snapshot, taskId);
    }

    @Override
    public void resetSnapshot(String snapshotId, String diskId) throws GCloudException {
        Snapshot snapshot = snapshotDao.getById(snapshotId);
        if (snapshot == null) {
            throw new GCloudException("0070302::找不到对应的快照");
        }
        Volume volume = volumeDao.getById(snapshot.getVolumeId());
        this.getProvider(snapshot.getProvider()).resetSnapshot(volume, snapshot, diskId);
    }

    private ISnapshotProvider getProvider(int providerType) {
        ISnapshotProvider provider = ResourceProviders.checkAndGet(ResourceType.SNAPSHOT, providerType);
        return provider;
    }

}
