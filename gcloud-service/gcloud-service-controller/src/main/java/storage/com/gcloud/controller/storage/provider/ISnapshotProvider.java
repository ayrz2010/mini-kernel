
package com.gcloud.controller.storage.provider;

import com.gcloud.controller.IResourceProvider;
import com.gcloud.controller.storage.entity.Snapshot;
import com.gcloud.controller.storage.entity.Volume;
import com.gcloud.header.api.model.CurrentUser;

public interface ISnapshotProvider extends IResourceProvider {

    void createSnapshot(Volume volume, String snapshotId, String name, String description, CurrentUser currentUser, String taskId);

    void updateSnapshot(String snapshotRefId, String name, String description);

    void deleteSnapshot(Volume volume, Snapshot snapshot, String taskId);

    void resetSnapshot(Volume volume, Snapshot snapshot, String diskId);

}
