
package com.gcloud.controller.storage.service;

import java.util.List;

import com.gcloud.core.exception.GCloudException;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.storage.model.DiskCategoryModel;
import com.gcloud.header.storage.model.StoragePoolModel;

public interface IStoragePoolService {

    List<DiskCategoryModel> describeDiskCategories(String zoneId);

    String createDiskCategory(String zoneId, String name, Integer minSize, Integer maxSize);

    PageResult<StoragePoolModel> describeStoragePools(int pageNumber, int pageSize);

    String createStoragePool(String displayName, Integer providerType, String storageType, String poolName, String zoneId, String categoryId) throws GCloudException;

    void modifyStoragePool(String poolId, String displayName) throws GCloudException;

    void deleteStoragePool(String poolId) throws GCloudException;

}
