package com.gcloud.controller.compute.service.vm.base.impl;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.compute.dao.InstanceDao;
import com.gcloud.controller.compute.dao.InstanceTypeDao;
import com.gcloud.controller.compute.dispatcher.Dispatcher;
import com.gcloud.controller.compute.entity.VmInstance;
import com.gcloud.controller.compute.handler.api.model.DescribeInstanceTypesParams;
import com.gcloud.controller.compute.handler.api.model.DescribeInstancesParams;
import com.gcloud.controller.compute.service.vm.base.IVmBaseService;
import com.gcloud.controller.network.dao.FloatingIpDao;
import com.gcloud.controller.network.dao.PortDao;
import com.gcloud.controller.network.entity.FloatingIp;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.compute.enums.VmState;
import com.gcloud.header.compute.enums.VmStepState;
import com.gcloud.header.compute.msg.api.model.InstanceAttributesType;
import com.gcloud.header.compute.msg.api.model.InstanceTypeItemType;
import com.gcloud.header.compute.msg.node.vm.base.ModifyPasswordMsg;
import com.gcloud.header.compute.msg.node.vm.base.RebootInstanceMsg;
import com.gcloud.header.compute.msg.node.vm.base.StartInstanceMsg;
import com.gcloud.header.compute.msg.node.vm.base.StopInstanceMsg;
import com.gcloud.header.network.model.IpAddressSetType;
import com.gcloud.header.network.model.NetworkInterfaceType;
import com.gcloud.header.network.model.NetworkInterfaces;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
@Transactional
public class VmBaseServiceImpl implements IVmBaseService {
    @Autowired
    private InstanceDao instanceDao;

    @Autowired
    private InstanceTypeDao instanceTypeDao;

    @Autowired
    private MessageBus bus;
    
    @Autowired
    private PortDao portDao;
    
    @Autowired
    private FloatingIpDao floatingIpDao;


    @Override
    public void startInstance(String instanceId) {
        startInstance(instanceId, true, true);
    }

    @Override
    public void startInstance(String instanceId, boolean inTask, boolean handleResource) {

        VmInstance vm = instanceDao.getById(instanceId);
        if (vm == null) {
            throw new GCloudException("0010202::找不到云服务器");
        }

        if (!instanceDao.updateInstanceStepState(instanceId, VmStepState.STARTING, inTask, VmState.STOPPED)) {
            throw new GCloudException("0010204::云服务器当前状态不能开机");
        }

        if(handleResource){
            Dispatcher.dispatcher().assignNode(vm.getHostname(), vm.getCore(), vm.getMemory());
        }

        StartInstanceMsg startInstanceMsg = new StartInstanceMsg();
        startInstanceMsg.setInstanceId(instanceId);
        startInstanceMsg.setServiceId(MessageUtil.computeServiceId(vm.getHostname()));
        startInstanceMsg.setHandleResource(handleResource);
        bus.send(startInstanceMsg);

    }

    @Override
    public void stopInstance(String instanceId) throws GCloudException {
        stopInstance(instanceId, true, true);
    }

    @Override
    public void stopInstance(String instanceId, boolean inTask, boolean handleResource) throws GCloudException {
        VmInstance vm = instanceDao.getById(instanceId);
        if (vm == null) {
            throw new GCloudException("0010302::找不到云服务器");
        }

        if (!instanceDao.updateInstanceStepState(instanceId, VmStepState.STOPPING, inTask, VmState.RUNNING)){
            throw new GCloudException("0010204::云服务器当前状态不能关机");
        }

        StopInstanceMsg stopInstanceMsg = new StopInstanceMsg();
        stopInstanceMsg.setInstanceId(vm.getId());
        stopInstanceMsg.setServiceId(MessageUtil.computeServiceId(vm.getHostname()));
        stopInstanceMsg.setHandleResource(handleResource);

        bus.send(stopInstanceMsg);

    }

    @Override
    public void rebootInstance(String instanceId, Boolean forceStop) throws GCloudException {
        rebootInstance(instanceId, true);
    }

    @Override
    public void rebootInstance(String instanceId, Boolean forceStop, boolean inTask) throws GCloudException {
        VmInstance vm = instanceDao.getById(instanceId);
        if (vm == null) {
            throw new GCloudException("0010402::找不到云服务器");
        }
        if (!instanceDao.updateInstanceStepState(instanceId, VmStepState.REBOOTING, inTask, VmState.RUNNING)){
            throw new GCloudException("0010404::云服务器当前状态不能重启");
        }

        RebootInstanceMsg rebootInstanceMsg = new RebootInstanceMsg();
        rebootInstanceMsg.setInstanceId(vm.getId());
        rebootInstanceMsg.setServiceId(MessageUtil.computeServiceId(vm.getHostname()));
        rebootInstanceMsg.setForceStop(forceStop);
        bus.send(rebootInstanceMsg);

    }

    @Override
    public List<InstanceTypeItemType> describeInstanceTypes(DescribeInstanceTypesParams params) {

        if (params == null) {
            params = new DescribeInstanceTypesParams();
        }

        return instanceTypeDao.describeInstanceTypes(params, InstanceTypeItemType.class);

    }

	@Override
	public void modifyInstanceAttribute(String instanceId, String instanceName, String password, String taskId) {
		VmInstance vm = instanceDao.getById(instanceId);
        if (vm == null) {
            throw new GCloudException("::找不到云服务器");
        }
        if(StringUtils.isNotBlank(instanceName)) {
        	List<String> updateFields = new ArrayList<String>();
        	updateFields.add(vm.updateAlias(instanceName));
        	instanceDao.update(vm, updateFields);
        }
        
        if(StringUtils.isNotBlank(password)) {
        	ModifyPasswordMsg modifyMsg = new ModifyPasswordMsg();
        	modifyMsg.setInstanceId(vm.getId());
        	modifyMsg.setServiceId(MessageUtil.computeServiceId(vm.getHostname()));
        	modifyMsg.setPassword(password);
        	if(StringUtils.isNotBlank(taskId)) {
        		modifyMsg.setTaskId(taskId);
        	}
            bus.send(modifyMsg);
        }
        
	}

	@Override
	public PageResult<InstanceAttributesType> describeInstances(DescribeInstancesParams params, CurrentUser currentUser) {
		PageResult<InstanceAttributesType> pages = instanceDao.describeInstances(params, currentUser, InstanceAttributesType.class);
		//加上浮动IP列表、网卡列表
		List<InstanceAttributesType> lists = pages.getList();
		for(InstanceAttributesType item:lists) {
			List<FloatingIp> ips = floatingIpDao.findByProperty("instanceId", item.getInstanceId());
            IpAddressSetType ipAddressSetType = new IpAddressSetType();
			List<String> ipAddrs = new ArrayList<>();
			for(FloatingIp ip : ips) {
                ipAddrs.add(ip.getFloatingIpAddress());
			}
            ipAddressSetType.setIpAddress(ipAddrs);
			item.setEipAddress(ipAddressSetType);

            NetworkInterfaces networkInterfaces = new NetworkInterfaces();
            networkInterfaces.setNetworkInterface(portDao.getInstanceNetworkInterfaces(item.getInstanceId(), NetworkInterfaceType.class));
			item.setNetworkInterfaces(networkInterfaces);
            if (item.getTaskState() != null) {
                item.setStatus(item.getTaskState());
            }
		}
		return pages;
	}

    @Override
    public void cleanState(String instanceId, Boolean inTask) {
        if(inTask != null && inTask){
            instanceDao.cleanStepState(instanceId);
        }else{
            instanceDao.cleanState(instanceId);
        }

    }
}
