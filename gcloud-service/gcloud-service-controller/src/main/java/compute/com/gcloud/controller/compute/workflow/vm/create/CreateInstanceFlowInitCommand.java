package com.gcloud.controller.compute.workflow.vm.create;

import com.gcloud.controller.compute.dao.InstanceDao;
import com.gcloud.controller.compute.dispatcher.Dispatcher;
import com.gcloud.controller.compute.workflow.model.vm.CreateInstanceFlowInitCommandReq;
import com.gcloud.controller.compute.workflow.model.vm.CreateInstanceFlowInitCommandRes;
import com.gcloud.core.service.SpringUtil;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;
import com.gcloud.header.compute.msg.api.model.DiskInfo;
import com.gcloud.header.storage.enums.DiskType;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
@Slf4j
public class CreateInstanceFlowInitCommand extends BaseWorkFlowCommand {

	@Override
	protected Object process() throws Exception {
		CreateInstanceFlowInitCommandReq req = (CreateInstanceFlowInitCommandReq) getReqParams();

		DiskInfo systemDisk = new DiskInfo();
		systemDisk.setDiskName(req.getSystemDiskName());
		systemDisk.setCategory(req.getSystemDiskCategory());
		systemDisk.setDiskType(DiskType.SYSTEM);
		systemDisk.setImageRef(req.getImageId());
		systemDisk.setZoneId(req.getZoneId());

		CreateInstanceFlowInitCommandRes res = new CreateInstanceFlowInitCommandRes();
		if(req.getPreRes() != null){

			systemDisk.setSize(req.getPreRes().getSystemDiskSize());

			res.setStorageType(req.getPreRes().getStorageType());
			res.setImageInfo(req.getPreRes().getImageInfo());
			res.setCreateHost(req.getPreRes().getCreateHost());
			res.setInstanceId(req.getPreRes().getId());
			res.setCpu(req.getPreRes().getCpu());
			res.setMemory(req.getPreRes().getMemory());
			res.setSystemDiskSize(req.getPreRes().getSystemDiskSize());
			res.setCreateUser(req.getPreRes().getCreateUser());
		}

		res.setInstanceName(req.getInstanceName());
		res.setSystemDisk(systemDisk);

		if(req.getDataDisk() != null && req.getDataDisk().size() > 0){
			for(DiskInfo diskInfo : req.getDataDisk()){
				if(StringUtils.isBlank(diskInfo.getZoneId())){
					diskInfo.setZoneId(req.getZoneId());
				}

			}
		}
		res.setDataDisk(req.getDataDisk());



		//如果有其他业务逻辑，跑错时需要调用rollback

		return res;
	}

	@Override
	protected Object rollback() throws Exception {
        CreateInstanceFlowInitCommandReq req = (CreateInstanceFlowInitCommandReq) getReqParams();

	    try{
            InstanceDao instanceDao = (InstanceDao) SpringUtil.getBean("instanceDao");
            instanceDao.deleteById(req.getPreRes().getId());
        }catch (Exception ex){
            log.error("创建云服务器回滚，删除虚拟机失败", ex);
        }


        try{
            Dispatcher.dispatcher().release(req.getPreRes().getCreateHost(), req.getCpu(), req.getMemory());
        }catch (Exception ex){
            log.error("创建云服务器回滚，释放资源失败", ex);
        }

		return null;
	}


	@Override
	protected Object timeout() throws Exception {
		CreateInstanceFlowInitCommandRes res = (CreateInstanceFlowInitCommandRes) getResParams();
		InstanceDao instanceDao = (InstanceDao) SpringUtil.getBean("instanceDao");
		/*
		 * if(vm.getState().equals(VmState)) {
		 * 
		 * }
		 */
		return null;
	}

	@Override
	protected Class<?> getReqParamClass() {
		return CreateInstanceFlowInitCommandReq.class;
	}

	@Override
	protected Class<?> getResParamClass() {
		return CreateInstanceFlowInitCommandRes.class;
	}

}
