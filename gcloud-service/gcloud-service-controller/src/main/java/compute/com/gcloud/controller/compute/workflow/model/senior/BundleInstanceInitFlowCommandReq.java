package com.gcloud.controller.compute.workflow.model.senior;

/**
 * Created by yaowj on 2018/11/30.
 */
public class BundleInstanceInitFlowCommandReq {

    private String instanceId;
    private String imageName;
    private Boolean inTask;

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public Boolean getInTask() {
        return inTask;
    }

    public void setInTask(Boolean inTask) {
        this.inTask = inTask;
    }
}
