package com.gcloud.controller.compute.workflow.vm.senior.bundle;

import com.gcloud.controller.compute.workflow.model.senior.BundleInstanceWorkflowReq;
import com.gcloud.controller.compute.service.vm.senior.IVmSeniorService;
import com.gcloud.core.workflow.core.BaseWorkFlows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Created by yaowj on 2018/12/3.
 */
@Component
@Scope("prototype")
@Slf4j
public class BundleInstanceWorkflow extends BaseWorkFlows {

    @Autowired
    private IVmSeniorService vmSeniorService;

    @Override
    public String getFlowTypeCode() {
        return "bundleInstanceWorkflow";
    }

    @Override
    public Object preProcess() {
        BundleInstanceWorkflowReq req = (BundleInstanceWorkflowReq)getReqParams();
        vmSeniorService.bundle(req.getInstanceId(), req.getInTask());
        return null;
    }

    @Override
    public void process() {

    }

    @Override
    protected Class<?> getReqParamClass() {
        return BundleInstanceWorkflowReq.class;
    }
}
