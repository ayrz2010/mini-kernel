package com.gcloud.controller.compute.workflow.vm.network;


import com.gcloud.controller.compute.service.vm.netowork.IVmNetworkService;
import com.gcloud.controller.compute.workflow.model.network.AttachPortWorkflowReq;
import com.gcloud.core.workflow.core.BaseWorkFlows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
@Slf4j
public class AttachPortWorkflow extends BaseWorkFlows {

	@Autowired
	private IVmNetworkService vmNetworkService;

	@Override
	public String getFlowTypeCode() {
		return "AttachPortWorkflow";
	}

	@Override
	public void process() {


	}

	@Override
	protected Class<?> getReqParamClass() {
		return AttachPortWorkflowReq.class;
	}

	@Override
	public Object preProcess() {
		AttachPortWorkflowReq req = (AttachPortWorkflowReq)getReqParams();
		vmNetworkService.attachPortInit(req.getInstanceId(), req.getNetworkInterfaceId(), req.getOvsBridgeId(), null, req.getInTask());
		return null;
	}

}
