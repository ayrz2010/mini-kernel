package com.gcloud.controller.compute.workflow.vm.storage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gcloud.controller.compute.dao.InstanceDao;
import com.gcloud.controller.compute.entity.VmInstance;
import com.gcloud.controller.compute.workflow.model.storage.DetachDataDiskFlowCommandReq;
import com.gcloud.controller.compute.workflow.model.storage.DetachDataDiskFlowCommandRes;
import com.gcloud.controller.storage.service.IVolumeService;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;
import com.gcloud.header.compute.msg.node.vm.storage.DetachDataDiskMsg;

import lombok.extern.slf4j.Slf4j;

@Component
@Scope("prototype")
@Slf4j
public class DetachDataDiskFlowCommand extends BaseWorkFlowCommand {

	@Autowired
	private MessageBus bus;

	@Autowired
	private InstanceDao instanceDao;

	@Autowired
	private IVolumeService volumeService;

	@Override
	protected Object process() throws Exception {
		DetachDataDiskFlowCommandReq req = (DetachDataDiskFlowCommandReq) getReqParams();
		log.debug(String.format("磁盘%s 挂载点：%s 卸载块设备开始", req.getVolumeId(), req.getAttachmentId()));

		volumeService.detachVolume(req.getVolumeId(), req.getAttachmentId());

		log.debug(String.format("磁盘%s 挂载点：%s 卸载块设备结束", req.getVolumeId(), req.getAttachmentId()));
		return null;
	}

	@Override
	protected Object rollback() throws Exception {

		DetachDataDiskFlowCommandReq req = (DetachDataDiskFlowCommandReq) getReqParams();

		VmInstance ins = instanceDao.getById(req.getInstanceId());

		DetachDataDiskMsg msg = new DetachDataDiskMsg();
		msg.setTaskId(getTaskId());
		msg.setInstanceId(req.getInstanceId());
		msg.setServiceId(MessageUtil.computeServiceId(ins.getHostname()));

		bus.send(msg);

		return null;
	}

	@Override
	protected Object timeout() throws Exception {
		return null;
	}

	@Override
	protected Class<?> getReqParamClass() {
		return DetachDataDiskFlowCommandReq.class;
	}

	@Override
	protected Class<?> getResParamClass() {
		return DetachDataDiskFlowCommandRes.class;
	}
}
