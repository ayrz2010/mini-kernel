
package com.gcloud.controller.compute.service.vm.zone;

import java.util.List;

import com.gcloud.framework.db.PageResult;
import com.gcloud.header.compute.msg.api.model.AvailableZone;

public interface IVmZoneService {

    PageResult<AvailableZone> describeZones(Integer pageNumber, Integer pageSize);

    void createZone(String zoneName);

    void updateComputeNodeZone(String zoneId, List<Integer> nodeIds);

}
