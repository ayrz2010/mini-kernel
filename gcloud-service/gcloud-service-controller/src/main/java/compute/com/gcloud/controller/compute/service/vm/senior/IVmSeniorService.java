package com.gcloud.controller.compute.service.vm.senior;

/**
 * Created by yaowj on 2018/12/3.
 */
public interface IVmSeniorService {

    void bundle(String instanceId, boolean inTask);

    void modifyInstanceInit(String instanceId, String instanceType, boolean inTask);
}
