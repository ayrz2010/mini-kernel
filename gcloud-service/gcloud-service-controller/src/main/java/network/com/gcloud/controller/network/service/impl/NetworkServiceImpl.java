package com.gcloud.controller.network.service.impl;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.ResourceProviders;
import com.gcloud.controller.network.dao.NetworkDao;
import com.gcloud.controller.network.entity.Network;
import com.gcloud.controller.network.provider.INetworkProvider;
import com.gcloud.controller.network.service.INetworkService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.enums.ResourceType;
import com.gcloud.header.model.VpcsItemType;
import com.gcloud.header.network.msg.api.CreateExternalNetworkMsg;
import com.gcloud.header.network.msg.api.CreateVpcMsg;
import com.gcloud.header.network.msg.api.DeleteVpcMsg;
import com.gcloud.header.network.msg.api.DescribeVpcsMsg;
import com.gcloud.header.network.msg.api.ModifyVpcAttributeMsg;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class NetworkServiceImpl implements INetworkService {
    @Autowired
    private NetworkDao networkDao;

    public PageResult<VpcsItemType> describeVpcs(DescribeVpcsMsg msg) {
        return networkDao.describeVpcs(msg);
    }

    @Override
    public String createNetwork(CreateVpcMsg msg) {
        String networkId = StringUtils.genUuid();
        this.getProviderOrDefault().createNetwork(networkId, msg);
        return networkId;
    }
    
    @Override
    public String createExternalNetwork(CreateExternalNetworkMsg msg) {
        String networkId = StringUtils.genUuid();
        this.getProviderOrDefault().createExternalNetwork(networkId, msg);
        return networkId;
    }

    @Override
    public void removeNetwork(DeleteVpcMsg msg) {
        Network network = networkDao.getById(msg.getVpcId());
        if (network != null) {
        	networkDao.deleteById(msg.getVpcId());
            this.checkAndGetProvider(network.getProvider()).removeNetwork(network.getProviderRefId());
        }
    }

    @Override
    public void updateNetwork(ModifyVpcAttributeMsg msg) {
        Network network = networkDao.getById(msg.getVpcId());
        if (network == null) {
            throw new GCloudException("0100203");
        }
        List<String> updateField = new ArrayList<String>();
        updateField.add(network.updateName(msg.getVpcName()));
        updateField.add(network.updateUpdatedAt(new Date()));
        networkDao.update(network, updateField);
       
        this.checkAndGetProvider(network.getProvider()).updateNetwork(network.getProviderRefId(), msg.getVpcName());
    
    }

    @Override
    public void getNetworks(String id) {
        // TODO Auto-generated method stub

    }

    private INetworkProvider getProviderOrDefault() {
        INetworkProvider provider = ResourceProviders.getDefault(ResourceType.NETWORK);
        return provider;
    }

    private INetworkProvider checkAndGetProvider(Integer providerType) {
        INetworkProvider provider = ResourceProviders.checkAndGet(ResourceType.NETWORK, providerType);
        return provider;
    }

}
