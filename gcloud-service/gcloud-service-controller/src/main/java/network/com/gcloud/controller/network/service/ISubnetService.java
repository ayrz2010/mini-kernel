package com.gcloud.controller.network.service;

import com.gcloud.controller.network.model.CreateVSwitchParams;
import com.gcloud.controller.network.model.DescribeVSwitchesParams;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.network.model.VSwitchSetType;

public interface ISubnetService {
	String createSubnet(CreateVSwitchParams params, CurrentUser currentUser);
	void deleteSubnet(String subnetId);
	void modifyAttribute(String subnetId, String subnetName);
	PageResult<VSwitchSetType> describeVSwitches(DescribeVSwitchesParams params, CurrentUser currentUser);
}
