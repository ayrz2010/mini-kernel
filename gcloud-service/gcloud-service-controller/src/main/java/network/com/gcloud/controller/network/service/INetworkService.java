package com.gcloud.controller.network.service;

import com.gcloud.framework.db.PageResult;
import com.gcloud.header.model.VpcsItemType;
import com.gcloud.header.network.msg.api.CreateExternalNetworkMsg;
import com.gcloud.header.network.msg.api.CreateVpcMsg;
import com.gcloud.header.network.msg.api.DeleteVpcMsg;
import com.gcloud.header.network.msg.api.DescribeVpcsMsg;
import com.gcloud.header.network.msg.api.ModifyVpcAttributeMsg;

public interface INetworkService {
	PageResult<VpcsItemType> describeVpcs(DescribeVpcsMsg msg);
	String createNetwork(CreateVpcMsg msg);
	String createExternalNetwork(CreateExternalNetworkMsg msg);
	void removeNetwork(DeleteVpcMsg msg);
	void updateNetwork(ModifyVpcAttributeMsg msg);
	void getNetworks(String id);
}
