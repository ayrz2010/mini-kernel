package com.gcloud.controller.network.provider;

import com.gcloud.controller.IResourceProvider;
import com.gcloud.controller.network.entity.Router;
import com.gcloud.header.network.msg.api.CreateVRouterMsg;

import java.util.List;
import java.util.Map;

public interface IRouterProvider extends IResourceProvider {

    String createVRouter(CreateVRouterMsg msg);

    void deleteVRouter(String routerRefId);

    void modifyVRouterAttribute(String routerRefId, String vRouterName);

    void setVRouterGateway(String routerRefId, String vpcId);

    void cleanVRouterGateway(String routerRefId);

    void attachSubnetRouter(String routerRefId, String subnetId);

    void detachSubnetRouter(String routerRefId, String subnetId);

    List<Router> list(Map<String, String> filter);
    
    org.openstack4j.model.network.Router getVRouter(String routerRefId);
}
