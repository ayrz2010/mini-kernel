package com.gcloud.controller.security.service;

import com.gcloud.controller.security.model.CreateClusterParams;
import com.gcloud.controller.security.model.CreateClusterResponse;
import com.gcloud.controller.security.model.DescribeSecurityClusterParams;
import com.gcloud.controller.security.model.EnableClusterHaParams;
import com.gcloud.controller.security.model.EnableClusterHaResponse;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.security.model.SecurityClusterType;

public interface ISecurityClusterService {

    CreateClusterResponse createCluster(CreateClusterParams ccp, CurrentUser currentUser);
    void delete(String id);
    void cleanClusterData(String id);

    EnableClusterHaResponse enableHa(EnableClusterHaParams params, CurrentUser currentUser);
    void disable(String clusterId, CurrentUser currentUser);
    void cleanClusterHaData(String clusterId);
    
    PageResult<SecurityClusterType> describeSecurityCluster(DescribeSecurityClusterParams params, CurrentUser currentUser);
}
