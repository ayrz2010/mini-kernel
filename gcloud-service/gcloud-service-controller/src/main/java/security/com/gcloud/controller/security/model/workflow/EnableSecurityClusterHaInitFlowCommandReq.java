package com.gcloud.controller.security.model.workflow;

import com.gcloud.controller.security.model.EnableClusterHaResponse;

public class EnableSecurityClusterHaInitFlowCommandReq {

    private EnableClusterHaResponse enableHaInfo;

    public EnableClusterHaResponse getEnableHaInfo() {
        return enableHaInfo;
    }

    public void setEnableHaInfo(EnableClusterHaResponse enableHaInfo) {
        this.enableHaInfo = enableHaInfo;
    }
}
