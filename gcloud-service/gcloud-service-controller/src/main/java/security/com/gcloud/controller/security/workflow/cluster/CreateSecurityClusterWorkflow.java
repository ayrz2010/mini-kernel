package com.gcloud.controller.security.workflow.cluster;

import com.gcloud.controller.security.model.CreateClusterParams;
import com.gcloud.controller.security.model.CreateClusterResponse;
import com.gcloud.controller.security.model.workflow.CreateSecurityClusterWorkflowReq;
import com.gcloud.controller.security.service.ISecurityClusterService;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.core.workflow.core.BaseWorkFlows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
@Slf4j
public class CreateSecurityClusterWorkflow extends BaseWorkFlows{

    @Autowired
    private ISecurityClusterService clusterService;


    @Override
    public String getFlowTypeCode() {
        return "createSecurityClusterWorkflow";
    }

    @Override
    public Object preProcess() {
        CreateSecurityClusterWorkflowReq req = (CreateSecurityClusterWorkflowReq)getReqParams();
        CreateClusterParams params = BeanUtil.copyBean(req, CreateClusterParams.class);

        CreateClusterResponse createResponse = clusterService.createCluster(params, req.getCurrentUser());

        return createResponse;
    }

    @Override
    public void process() {


    }

    @Override
    public boolean judgeExecute() {
        return super.judgeExecute();
    }

    @Override
    protected Class<?> getReqParamClass() {
        return CreateSecurityClusterWorkflowReq.class;
    }
}
