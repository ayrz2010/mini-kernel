package com.gcloud.controller.security.model.workflow;

public class DisableSecurityClusterHaInitFlowCommandReq {

    private String clusterId;

    public String getClusterId() {
        return clusterId;
    }

    public void setClusterId(String clusterId) {
        this.clusterId = clusterId;
    }
}
