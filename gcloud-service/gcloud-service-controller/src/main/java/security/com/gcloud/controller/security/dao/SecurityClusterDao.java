package com.gcloud.controller.security.dao;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.security.entity.SecurityCluster;
import com.gcloud.controller.security.model.DescribeSecurityClusterParams;
import com.gcloud.core.currentUser.policy.enums.UserResourceFilterPolicy;
import com.gcloud.core.currentUser.policy.model.FilterPolicyModel;
import com.gcloud.core.currentUser.policy.service.IUserResourceFilterPolicy;
import com.gcloud.core.service.SpringUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.framework.db.jdbc.annotation.Jdbc;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.security.model.SecurityClusterType;

@Jdbc("controllerJdbcTemplate")
@Repository
public class SecurityClusterDao extends JdbcBaseDaoImpl<SecurityCluster, String> {

    public int changeState(String id, String state){
        String sql = "update gc_security_cluster set state = ?, update_time = now() where id = ?";
        Object[] values = {state, id};
        return this.jdbcTemplate.update(sql, values);
    }
    
    public <E> PageResult<E> describeSecurityCluster(DescribeSecurityClusterParams params, Class<E> clazz, CurrentUser currentUser) {
    	
    	IUserResourceFilterPolicy filterPolicy = (IUserResourceFilterPolicy)SpringUtil.getBean(UserResourceFilterPolicy.TYPICAL.getFilterPolicyClazz());
		FilterPolicyModel sqlModel = filterPolicy.filterPolicy(currentUser, "i.");
		
		StringBuffer sql = new StringBuffer();
		List<Object> values = new ArrayList<>();
		
		Integer pageSize = params.getPageSize();
		boolean allFlag = false;
		if(pageSize == -1) {
			allFlag = true;
		}
		
		sql.append("select i.* from gc_security_cluster as i");
		sql.append(" where 1 = 1");
		
		if(StringUtils.isNotBlank(params.getSecurityClusterName())) {
			sql.append(" and i.name like concat('%', ?, '%')");
			values.add(params.getSecurityClusterName());
		}
		
		sql.append(sqlModel.getWhereSql());
		values.addAll(sqlModel.getParams());
		
		sql.append(" order by i.create_time desc");
		
		//是否查询全部
		if(allFlag) {
			List<SecurityClusterType> result = new ArrayList<SecurityClusterType>();
			List<SecurityCluster> sqlResponse = findBySql(sql.toString(), values);
			DateFormat dti = DateFormat.getDateTimeInstance();
			for (SecurityCluster item : sqlResponse) {
				SecurityClusterType temp = new SecurityClusterType();
				temp.setId(item.getId());
				temp.setName(item.getName());
				temp.setState(item.getState());
				temp.setCreateUser(item.getCreateUser());
				Date tempDate = item.getCreateTime();
				temp.setCreateTime(tempDate == null ? null : dti.format(tempDate));
				result.add(temp);
			}
			PageResult<E> page = new PageResult<E>();
			page.setList((List<E>) result);
			page.setPageNo(1);
			page.setPageSize(result.size());
			page.setTotalCount(result.size());
			return page;	
		} else {
			return findBySql(sql.toString(), values, params.getPageNumber(), params.getPageSize(), clazz);
		}
    }

}
