package com.gcloud.controller.security.model.workflow;

import com.gcloud.controller.security.model.CreateClusterResponse;

public class CreateSecurityClusterInitFlowCommandReq {

    private CreateClusterResponse createClusterInfo;

    public CreateClusterResponse getCreateClusterInfo() {
        return createClusterInfo;
    }

    public void setCreateClusterInfo(CreateClusterResponse createClusterInfo) {
        this.createClusterInfo = createClusterInfo;
    }
}
