package com.gcloud.controller.security.workflow.cluster;

import com.gcloud.controller.security.model.workflow.DeleteSecurityClusterWorkflowReq;
import com.gcloud.controller.security.service.ISecurityClusterService;
import com.gcloud.core.workflow.core.BaseWorkFlows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
@Slf4j
public class DeleteSecurityClusterWorkflow extends BaseWorkFlows {

    @Autowired
    private ISecurityClusterService securityClusterService;

    @Override
    public String getFlowTypeCode() {
        return "DeleteSecurityClusterWorkflow";
    }

    @Override
    public Object preProcess() {
        DeleteSecurityClusterWorkflowReq req = (DeleteSecurityClusterWorkflowReq)getReqParams();
        securityClusterService.delete(req.getId());
        return null;
    }

    @Override
    public void process() {

    }

    @Override
    protected Class<?> getReqParamClass() {
        return DeleteSecurityClusterWorkflowReq.class;
    }
}
