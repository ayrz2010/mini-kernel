package com.gcloud.controller.security.workflow.cluster;

import com.gcloud.controller.security.model.workflow.DisableSecurityClusterHaWorkflowReq;
import com.gcloud.controller.security.service.ISecurityClusterService;
import com.gcloud.core.workflow.core.BaseWorkFlows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
@Slf4j
public class DisableSecurityClusterHaWorkflow extends BaseWorkFlows {

    @Autowired
    private ISecurityClusterService securityClusterService;

    @Override
    public String getFlowTypeCode() {
        return "DisableSecurityClusterHaWorkflow";
    }

    @Override
    public Object preProcess() {

        DisableSecurityClusterHaWorkflowReq req = (DisableSecurityClusterHaWorkflowReq)getReqParams();
        securityClusterService.disable(req.getId(), req.getCurrentUser());

        return null;
    }

    @Override
    public void process() {

    }

    @Override
    protected Class<?> getReqParamClass() {
        return DisableSecurityClusterHaWorkflowReq.class;
    }
}
