package com.gcloud.controller.security.handler.api.cluster;

import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.security.msg.api.cluster.ApiCreateSecurityClusterMsg;
import com.gcloud.header.security.msg.api.cluster.ApiCreateSecurityClusterReplyMsg;

@ApiHandler(module = Module.ECS, subModule= SubModule.SECURITYCLUSTER, action = "DescribeSecurityClusterComponent")
public class ApiDescribeSecurityClusterComponentHandler extends MessageHandler<ApiCreateSecurityClusterMsg, ApiCreateSecurityClusterReplyMsg> {

    @Override
    public ApiCreateSecurityClusterReplyMsg handle(ApiCreateSecurityClusterMsg msg) throws GCloudException {
        return null;
    }
}
