package com.gcloud.controller.log.dao;

import com.gcloud.controller.log.entity.Log;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
@Repository
public class LogDao extends JdbcBaseDaoImpl<Log, Long> {
	public Log findTask(String taskId,String objectId) {
		StringBuffer sql = new StringBuffer();
		List<Object> parameterValues = new ArrayList<Object>();
		sql.append("select * from gc_log where task_id = ?");
		parameterValues.add(taskId);
		if(null != objectId) {
			sql.append(" and object_id=?");
			parameterValues.add(objectId);
		}
		List<Log> logs = findBySql(sql.toString(), parameterValues);
		if(logs != null && logs.size() > 0)
			return logs.get(0);
		return null;
	}
}
