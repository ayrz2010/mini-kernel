package com.gcloud.compute.virtual.libvirt.volume;

import org.dom4j.Document;

import com.gcloud.header.storage.model.VmVolumeDetail;

public interface IVmVolume {

	void addDiskDevice(Document doc, VmVolumeDetail volumeInfo, boolean isCreateElement);

	void createDiskFile(String imageId, String volumeId, Integer systemSize);

}
