package com.gcloud.header.image.msg.api;

import com.gcloud.header.ApiPageMessage;

/**
 * Created by yaowj on 2018/9/21.
 */
public class ApiDescribeImagesMsg extends ApiPageMessage {

    private static final long serialVersionUID = 1L;

    @Override
    public Class replyClazz() {
        return ApiDescribeImagesReplyMsg.class;
    }

    private String imageId;
    private String imageName;
    private String status;

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
