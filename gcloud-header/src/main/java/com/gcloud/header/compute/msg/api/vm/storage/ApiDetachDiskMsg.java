package com.gcloud.header.compute.msg.api.vm.storage;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;

public class ApiDetachDiskMsg extends ApiMessage {
	@ApiModel(description = "虚拟机ID", require = true)
	private String instanceId;
	@ApiModel(description = "磁盘ID", require = true)
	private String diskId;

	@Override
	public Class replyClazz() {
		return ApiDetachDiskReplyMsg.class;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public String getDiskId() {
		return diskId;
	}

	public void setDiskId(String diskId) {
		this.diskId = diskId;
	}
}
