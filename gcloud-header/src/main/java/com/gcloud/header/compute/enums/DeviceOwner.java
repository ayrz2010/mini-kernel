package com.gcloud.header.compute.enums;

public enum DeviceOwner {
	COMPUTE("compute:node"),FOREIGN("network:foreign");
	
	private String value;
	
	DeviceOwner (String value){
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
