
package com.gcloud.header.compute.msg.api.vm.zone;

import java.util.List;

import com.gcloud.header.ApiMessage;

public class ApiUpdateComputeNodeZoneMsg extends ApiMessage {

    private static final long serialVersionUID = 1L;

    private String zoneId;
    private List<Integer> nodeIds;

    @Override
    public Class replyClazz() {
        return ApiUpdateComputeNodeZoneReplyMsg.class;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public List<Integer> getNodeIds() {
        return nodeIds;
    }

    public void setNodeIds(List<Integer> nodeIds) {
        this.nodeIds = nodeIds;
    }

}
