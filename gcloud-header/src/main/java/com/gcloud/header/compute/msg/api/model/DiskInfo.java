package com.gcloud.header.compute.msg.api.model;

import com.gcloud.header.api.ApiModel;
import com.gcloud.header.storage.enums.DiskType;
import org.hibernate.validator.constraints.Range;

import java.io.Serializable;

public class DiskInfo implements Serializable {
	
	@ApiModel(description = "数据盘大小")
	@Range(min=10,max=32768,message="0010108::系统盘大小[10,32768],单位GB")
	private Integer size;
	@ApiModel(description = "磁盘种类")
	private String category;
	
	private String snapshotId;
	private String diskName;
	private String description;
	private DiskType diskType;
	private String imageRef;
	private String zoneId;

	public String getImageRef() {
		return imageRef;
	}

	public void setImageRef(String imageRef) {
		this.imageRef = imageRef;
	}

	public DiskType getDiskType() {
		return diskType;
	}

	public void setDiskType(DiskType diskType) {
		this.diskType = diskType;
	}

	public String getSnapshotId() {
		return snapshotId;
	}

	public void setSnapshotId(String snapshotId) {
		this.snapshotId = snapshotId;
	}

	public String getDiskName() {
		return diskName;
	}

	public void setDiskName(String diskName) {
		this.diskName = diskName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getZoneId() {
		return zoneId;
	}

	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}
}
