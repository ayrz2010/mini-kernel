package com.gcloud.header.compute.msg.api.trash;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.ApiReplyMessage;

/**
 * Created by yaowj on 2018/12/3.
 */
public class ApiDeleteInstanceMsg extends ApiMessage {

    private String instanceId;

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    @Override
    public Class replyClazz() {
        return ApiReplyMessage.class;
    }
}
