package com.gcloud.header.identity.tenant;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.ApiReplyMessage;

public class DeleteTenantMsg extends ApiMessage{
	private String id;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	@Override
	public Class replyClazz() {
		return ApiReplyMessage.class;
	}
}
