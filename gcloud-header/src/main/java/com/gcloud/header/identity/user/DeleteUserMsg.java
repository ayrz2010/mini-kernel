package com.gcloud.header.identity.user;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.ApiReplyMessage;

public class DeleteUserMsg extends ApiMessage {
	private String id;
	@Override
	public Class replyClazz() {
		return ApiReplyMessage.class;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

}
