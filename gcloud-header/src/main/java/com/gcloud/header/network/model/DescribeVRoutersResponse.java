package com.gcloud.header.network.model;

import java.io.Serializable;
import java.util.List;

public class DescribeVRoutersResponse implements Serializable{
	private List<VRouterSetType> routers;

	public List<VRouterSetType> getRouters() {
		return routers;
	}

	public void setRouters(List<VRouterSetType> routers) {
		this.routers = routers;
	}
}
