package com.gcloud.header.network.msg.api;

import java.util.List;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.network.model.DescribeSecurityGroupResponse;
import com.gcloud.header.network.model.SecurityGroupItemType;

public class DescribeSecurityGroupsReplyMsg extends PageReplyMessage<SecurityGroupItemType>{
	private DescribeSecurityGroupResponse securityGroups;
	@Override
	public void setList(List<SecurityGroupItemType> list) {
		securityGroups = new DescribeSecurityGroupResponse();
		securityGroups.setSecurityGroup(list);
		
	}
	public DescribeSecurityGroupResponse getSecurityGroups() {
		return securityGroups;
	}
	public void setSecurityGroups(DescribeSecurityGroupResponse securityGroups) {
		this.securityGroups = securityGroups;
	}
	
}
