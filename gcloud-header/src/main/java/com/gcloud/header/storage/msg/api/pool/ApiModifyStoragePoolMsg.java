
package com.gcloud.header.storage.msg.api.pool;

import com.gcloud.header.ApiMessage;

public class ApiModifyStoragePoolMsg extends ApiMessage {

    private static final long serialVersionUID = 1L;

    @Override
    public Class replyClazz() {
        return ApiModifyStoragePoolReplyMsg.class;
    }

    private String poolId;
    private String displayName;

    public String getDisplayName() {
        return displayName;
    }

    public String getPoolId() {
        return poolId;
    }

    public void setPoolId(String poolId) {
        this.poolId = poolId;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

}
