package com.gcloud.header.storage.msg.api.volume;

import com.gcloud.header.ApiPageMessage;

/**
 * Created by yaowj on 2018/9/29.
 */
public class ApiDescribeDisksMsg extends ApiPageMessage {

    private static final long serialVersionUID = 1L;

    private String diskType;
    private String status;
    private String diskName;
    private String instanceId;

    @Override
    public Class replyClazz() {
        return ApiDescribeDisksReplyMsg.class;
    }

    public String getDiskType() {
        return diskType;
    }

    public void setDiskType(String diskType) {
        this.diskType = diskType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDiskName() {
        return diskName;
    }

    public void setDiskName(String diskName) {
        this.diskName = diskName;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }
}
