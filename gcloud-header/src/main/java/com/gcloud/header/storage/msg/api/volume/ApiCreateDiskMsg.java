package com.gcloud.header.storage.msg.api.volume;

import com.gcloud.header.ApiMessage;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;

/**
 * Created by yaowj on 2018/9/21.
 */
public class ApiCreateDiskMsg extends ApiMessage {

    private static final long serialVersionUID = 1L;

    @Override
    public Class replyClazz() {
        return ApiCreateDiskReplyMsg.class;
    }

    @Length(max = 255, message = "0060105::名称长度不能大于255")
    private String diskName;
    @Length(max = 255, message = "0060106::描述长度不能大于255")
    private String description;
    @Min(value = 1, message = "0060101::磁盘大小要大于1")
    private Integer size;
    private String snapshotId;
    private String diskCategory;
    private String zoneId;

    public String getDiskName() {
        return diskName;
    }

    public void setDiskName(String diskName) {
        this.diskName = diskName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public String getSnapshotId() {
        return snapshotId;
    }

    public void setSnapshotId(String snapshotId) {
        this.snapshotId = snapshotId;
    }

    public String getDiskCategory() {
        return diskCategory;
    }

    public void setDiskCategory(String diskCategory) {
        this.diskCategory = diskCategory;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }
}
