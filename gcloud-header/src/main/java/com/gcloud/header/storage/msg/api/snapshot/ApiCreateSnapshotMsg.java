package com.gcloud.header.storage.msg.api.snapshot;

import com.gcloud.header.ApiMessage;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by yaowj on 2018/9/21.
 */
public class ApiCreateSnapshotMsg extends ApiMessage {

    private static final long serialVersionUID = 1L;

    @Override
    public Class replyClazz() {
        return ApiCreateSnapshotReplyMsg.class;
    }

    @NotBlank(message = "0070101::磁盘ID不能为空")
    private String diskId;
    @Length(max = 255, message = "0070103::名称长度不能大于255")
    private String snapshotName;
    @Length(max = 255, message = "0070104::描述长度不能大于255")
    private String description;

    public String getDiskId() {
        return diskId;
    }

    public void setDiskId(String diskId) {
        this.diskId = diskId;
    }

    public String getSnapshotName() {
        return snapshotName;
    }

    public void setSnapshotName(String snapshotName) {
        this.snapshotName = snapshotName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
