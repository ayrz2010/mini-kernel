package com.gcloud.header.storage.msg.api.volume;

import com.gcloud.header.ApiReplyMessage;

/**
 * Created by yaowj on 2018/9/21.
 */
public class ApiCreateDiskReplyMsg extends ApiReplyMessage {


    private static final long serialVersionUID = 1L;

    private String diskId;

    public String getDiskId() {
        return diskId;
    }

    public void setDiskId(String diskId) {
        this.diskId = diskId;
    }
}
