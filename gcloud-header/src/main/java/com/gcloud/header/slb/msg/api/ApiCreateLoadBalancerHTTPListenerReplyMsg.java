package com.gcloud.header.slb.msg.api;

import com.gcloud.header.ApiReplyMessage;

public class ApiCreateLoadBalancerHTTPListenerReplyMsg extends ApiReplyMessage{
	
	private String listenerId;

	public String getListenerId() {
		return listenerId;
	}

	public void setListenerId(String listenerId) {
		this.listenerId = listenerId;
	}

}
