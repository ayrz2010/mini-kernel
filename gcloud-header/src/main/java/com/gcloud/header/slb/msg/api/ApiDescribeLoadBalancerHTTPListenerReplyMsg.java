package com.gcloud.header.slb.msg.api;

import com.gcloud.header.ApiReplyMessage;

public class ApiDescribeLoadBalancerHTTPListenerReplyMsg extends ApiReplyMessage {

	private Integer listenerPort;
	private String  status;
	private String vServerGroupId;
	
	public Integer getListenerPort() {
		return listenerPort;
	}
	public void setListenerPort(Integer listenerPort) {
		this.listenerPort = listenerPort;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getvServerGroupId() {
		return vServerGroupId;
	}
	public void setvServerGroupId(String vServerGroupId) {
		this.vServerGroupId = vServerGroupId;
	}
	
	
}
