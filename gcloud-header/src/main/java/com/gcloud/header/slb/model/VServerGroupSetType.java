package com.gcloud.header.slb.model;

import java.io.Serializable;

public class VServerGroupSetType implements Serializable{
	private String vServerGroupId;
	private String vServerGroupName;
	private String vServerGroupProtocol;
	public String getvServerGroupId() {
		return vServerGroupId;
	}
	public void setvServerGroupId(String vServerGroupId) {
		this.vServerGroupId = vServerGroupId;
	}
	public String getvServerGroupName() {
		return vServerGroupName;
	}
	public void setvServerGroupName(String vServerGroupName) {
		this.vServerGroupName = vServerGroupName;
	}
	public String getvServerGroupProtocol() {
		return vServerGroupProtocol;
	}
	public void setvServerGroupProtocol(String vServerGroupProtocol) {
		this.vServerGroupProtocol = vServerGroupProtocol;
	}
}
