package com.gcloud.header.slb.msg.api;

import com.gcloud.header.ApiMessage;

public class ApiSetVServerGroupBackendServersMsg extends ApiMessage {
	private String vServerGroupId;
	private String backendServers;
	@Override
	public Class replyClazz() {
		// TODO Auto-generated method stub
		return ApiSetVServerGroupBackendServersReplyMsg.class;
	}
	public String getvServerGroupId() {
		return vServerGroupId;
	}
	public void setvServerGroupId(String vServerGroupId) {
		this.vServerGroupId = vServerGroupId;
	}
	public String getBackendServers() {
		return backendServers;
	}
	public void setBackendServers(String backendServers) {
		this.backendServers = backendServers;
	}

}
