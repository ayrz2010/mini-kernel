package com.gcloud.header.slb.msg.api;

import org.hibernate.validator.constraints.NotBlank;

import com.gcloud.header.ApiMessage;

public class ApiDescribeSchedulerAttributeMsg extends ApiMessage {

	@NotBlank(message = "0130201::资源ID不能为空")
	private String resourceId;
	private String protocol;

	public String getResourceId() {
		return resourceId;
	}



	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}



	public String getProtocol() {
		return protocol;
	}



	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}



	@Override
	public Class replyClazz() {
		// TODO Auto-generated method stub
		return ApiDescribeSchedulerAttributeReplyMsg.class;
	}

}
