package com.gcloud.header.slb.msg.api;

import com.gcloud.header.ApiMessage;

public class ApiDescribeVServerGroupAttributeMsg extends ApiMessage {
	private String vServerGroupId;
	@Override
	public Class replyClazz() {
		// TODO Auto-generated method stub
		return ApiDescribeVServerGroupAttributeReplyMsg.class;
	}
	public String getvServerGroupId() {
		return vServerGroupId;
	}
	public void setvServerGroupId(String vServerGroupId) {
		this.vServerGroupId = vServerGroupId;
	}

}
