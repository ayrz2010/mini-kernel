package com.gcloud.header.slb.msg.api;

import org.hibernate.validator.constraints.NotBlank;

import com.gcloud.header.ApiMessage;

public class ApiSetSchedulerAttributeMsg extends ApiMessage {

	@NotBlank(message = "0130101::资源ID不能为空")
	private String resourceId;
	private String protocol;
	@NotBlank(message = "0130102::调度策略名称不能为空")
	private String scheduler;
	/**
	 * 调度策略 <br>
	 * 阿里：取值：wrr | wlc | rr <br>
	 * openstack：取值：ROUND_ROBIN | LEAST_CONNECTIONS | SOURCE_IP
	 */

	public String getResourceId() {
		return resourceId;
	}



	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}



	public String getProtocol() {
		return protocol;
	}



	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}



	public String getScheduler() {
		return scheduler;
	}



	public void setScheduler(String scheduler) {
		this.scheduler = scheduler;
	}



	@Override
	public Class replyClazz() {
		// TODO Auto-generated method stub
		return ApiSetSchedulerAttributeReplyMsg.class;
	}

}
