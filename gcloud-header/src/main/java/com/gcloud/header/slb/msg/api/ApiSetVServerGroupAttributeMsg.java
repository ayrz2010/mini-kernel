package com.gcloud.header.slb.msg.api;

import com.gcloud.header.ApiMessage;

public class ApiSetVServerGroupAttributeMsg extends ApiMessage {
	private String vServerGroupId;
	private String vServerGroupName;
	@Override
	public Class replyClazz() {
		// TODO Auto-generated method stub
		return ApiSetVServerGroupAttributeReplyMsg.class;
	}
	public String getvServerGroupId() {
		return vServerGroupId;
	}
	public void setvServerGroupId(String vServerGroupId) {
		this.vServerGroupId = vServerGroupId;
	}
	public String getvServerGroupName() {
		return vServerGroupName;
	}
	public void setvServerGroupName(String vServerGroupName) {
		this.vServerGroupName = vServerGroupName;
	}

}
