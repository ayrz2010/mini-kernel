package com.gcloud.header.slb.msg.api;

import com.gcloud.header.ApiReplyMessage;

public class ApiCreateVServerGroupReplyMsg extends ApiReplyMessage {
	private String vServerGroupId;

	public String getvServerGroupId() {
		return vServerGroupId;
	}

	public void setvServerGroupId(String vServerGroupId) {
		this.vServerGroupId = vServerGroupId;
	}
}
