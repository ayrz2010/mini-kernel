package com.gcloud.header.slb.msg.api;

import org.hibernate.validator.constraints.NotBlank;

import com.gcloud.header.ApiMessage;

public class ApiCreateVServerGroupMsg extends ApiMessage{
	@NotBlank(message = "0010101::负载均衡器ID不能为空")
	private String loadBalancerId;
	@NotBlank(message = "0140002::后端服务器组名称不能为空")
	private String vServerGroupName;
	@NotBlank(message = "0010101::镜像ID不能为空")
	private String vServerGroupProtocol;
	@Override
	public Class replyClazz() {
		// TODO Auto-generated method stub
		return ApiCreateVServerGroupReplyMsg.class;
	}
	public String getLoadBalancerId() {
		return loadBalancerId;
	}
	public void setLoadBalancerId(String loadBalancerId) {
		this.loadBalancerId = loadBalancerId;
	}
	public String getvServerGroupName() {
		return vServerGroupName;
	}
	public void setvServerGroupName(String vServerGroupName) {
		this.vServerGroupName = vServerGroupName;
	}
	public String getvServerGroupProtocol() {
		return vServerGroupProtocol;
	}
	public void setvServerGroupProtocol(String vServerGroupProtocol) {
		this.vServerGroupProtocol = vServerGroupProtocol;
	}

}
