
USE gcloud_controller;

-- gcloud多实现    2019-05-21

ALTER TABLE `gc_routers` ADD `provider` int(10) DEFAULT null;
ALTER TABLE `gc_routers` ADD `provider_ref_id` varchar(40) DEFAULT null;
ALTER TABLE `gc_routers` ADD `updated_at` datetime DEFAULT null;

ALTER TABLE `gc_subnets` ADD `provider` int(10) DEFAULT null;
ALTER TABLE `gc_subnets` ADD `provider_ref_id` varchar(40) DEFAULT null;
ALTER TABLE `gc_subnets` ADD `updated_at` datetime DEFAULT null;

ALTER TABLE `gc_security_groups` ADD `provider` int(10) DEFAULT null;
ALTER TABLE `gc_security_groups` ADD `provider_ref_id` varchar(40) DEFAULT null;
ALTER TABLE `gc_security_groups` ADD `updated_at` datetime DEFAULT null;

ALTER TABLE `gc_floating_ips` ADD `provider` int(10) DEFAULT null;
ALTER TABLE `gc_floating_ips` ADD `provider_ref_id` varchar(40) DEFAULT null;
ALTER TABLE `gc_floating_ips` ADD `updated_at` datetime DEFAULT null;

ALTER TABLE `gc_networks` ADD `provider` int(10) DEFAULT null;
ALTER TABLE `gc_networks` ADD `provider_ref_id` varchar(40) DEFAULT null;
ALTER TABLE `gc_networks` ADD `updated_at` datetime DEFAULT null;

ALTER TABLE `gc_ports` ADD `provider` int(10) DEFAULT null;
ALTER TABLE `gc_ports` ADD `provider_ref_id` varchar(40) DEFAULT null;
ALTER TABLE `gc_ports` ADD `updated_at` datetime DEFAULT null;

ALTER TABLE `gc_slb` ADD `provider` int(10) DEFAULT null;
ALTER TABLE `gc_slb` ADD `provider_ref_id` varchar(40) DEFAULT null;
ALTER TABLE `gc_slb` ADD `updated_at` datetime DEFAULT null;

ALTER TABLE `gc_images` ADD `provider` int(10) DEFAULT null;
ALTER TABLE `gc_images` ADD `provider_ref_id` varchar(40) DEFAULT null;
ALTER TABLE `gc_images` ADD `updated_at` datetime DEFAULT null;

ALTER TABLE `gc_snapshots` ADD `provider` int(10) DEFAULT null;
ALTER TABLE `gc_snapshots` ADD `provider_ref_id` varchar(40) DEFAULT null;
ALTER TABLE `gc_snapshots` ADD `updated_at` datetime DEFAULT null;

ALTER TABLE `gc_volumes` ADD `provider` int(10) DEFAULT null;
ALTER TABLE `gc_volumes` ADD `provider_ref_id` varchar(40) DEFAULT null;
ALTER TABLE `gc_volumes` ADD `updated_at` datetime DEFAULT null;

ALTER TABLE `gc_storage_pools` ADD `provider` int(10) DEFAULT null;
ALTER TABLE `gc_storage_pools` ADD `provider_ref_id` varchar(40) DEFAULT null;

ALTER TABLE `gc_ovs_bridges` ADD `provider` int(10) DEFAULT null;
ALTER TABLE `gc_ovs_bridges` ADD `provider_ref_id` varchar(40) DEFAULT null;


-- gcloud磁盘类型    2019-06-05

DROP TABLE IF EXISTS `gc_disk_categories`;
CREATE TABLE `gc_disk_categories` (
  `id` varchar(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `min_size` int(10) DEFAULT null,
  `max_size` int(10) DEFAULT null,
  `zone_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `gc_storage_pools` ADD `category_id` varchar(36) DEFAULT NULL;

ALTER TABLE `gc_volumes` ADD `pool_id` varchar(36) DEFAULT NULL;

-- gcloud可用区    2019-06-05

DROP TABLE IF EXISTS `gc_zones`;
CREATE TABLE `gc_zones` (
  `id` varchar(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `gc_subnets` ADD `zone_id` varchar(36) DEFAULT NULL;
ALTER TABLE `gc_compute_nodes` ADD `zone_id` varchar(36) DEFAULT NULL;
ALTER TABLE `gc_instances` ADD `zone_id` varchar(36) DEFAULT NULL;
ALTER TABLE `gc_instance_types` ADD `zone_id` varchar(36) DEFAULT NULL;
ALTER TABLE `gc_volumes` ADD `zone_id` varchar(36) DEFAULT NULL;
ALTER TABLE `gc_storage_pools` ADD `zone_id` varchar(36) DEFAULT NULL;

-- ovs_bridge_id 2019-06-10
alter table gc_ports change custom_ovs_br ovs_bridge_id varchar(200) null;
alter table gc_ovs_bridges_usage modify id bigint(20) auto_increment;

-- 虚拟机表租户ID    2019-06-17
ALTER TABLE `gc_instances` ADD `tenant_id` varchar(64) DEFAULT null;
ALTER TABLE `gc_images` ADD `tenant_id` varchar(64) DEFAULT NULL COMMENT '租户ID';
ALTER TABLE `gc_volumes` ADD `tenant_id` varchar(64) DEFAULT NULL COMMENT '租户ID';
ALTER TABLE `gc_snapshots` ADD `tenant_id` varchar(64) DEFAULT NULL COMMENT '租户ID';
ALTER TABLE `gc_floating_ips` ADD `tenant_id` varchar(64) DEFAULT NULL COMMENT '租户ID';
ALTER TABLE `gc_networks` ADD `tenant_id` varchar(64) DEFAULT NULL COMMENT '租户ID';
ALTER TABLE `gc_routers` ADD `tenant_id` varchar(64) DEFAULT NULL COMMENT '租户ID';
ALTER TABLE `gc_subnets` ADD `tenant_id` varchar(64) DEFAULT NULL COMMENT '租户ID';
ALTER TABLE `gc_security_groups` ADD `tenant_id` varchar(64) DEFAULT NULL COMMENT '租户ID';
ALTER TABLE `gc_slb` ADD `tenant_id` varchar(64) DEFAULT NULL COMMENT '租户ID';
ALTER TABLE `gc_slb` ADD `user_id` varchar(64) DEFAULT NULL;

-- 网卡表租户ID    2019-06-18
ALTER TABLE `gc_ports` ADD `tenant_id` varchar(64) DEFAULT NULL COMMENT '租户ID';


-- 负载均衡服务器组库表   2019-06-19
DROP TABLE IF EXISTS `gc_slb_vservergroup`;
CREATE TABLE `gc_slb_vservergroup` (
  `id` varchar(64) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `load_balancer_id` varchar(64) DEFAULT NULL,
  `protocol` varchar(64) DEFAULT NULL,
  `provider` tinyint(1) DEFAULT NULL,
  `provider_ref_id` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 浮动IP表qos、带宽字段   2019-06-19
ALTER TABLE `gc_floating_ips` ADD `bw_qos_policy_id` varchar(64) DEFAULT NULL COMMENT '带宽qos策略';
ALTER TABLE `gc_floating_ips` ADD `bandwidth` bigint(10) DEFAULT NULL COMMENT '带宽';
ALTER TABLE `gc_floating_ips` DROP column `routerId`;
ALTER TABLE `gc_floating_ips` ADD `router_id` varchar(64) DEFAULT NULL COMMENT '路由id';

-- 安全组规则表   2019-06-21
-- ----------------------------
--  Table structure for `gc_security_group_rules`
-- ----------------------------
DROP TABLE IF EXISTS `gc_security_group_rules`;
CREATE TABLE `gc_security_group_rules` (
  `id` varchar(64) NOT NULL COMMENT 'ID',
  `security_group_id` varchar(64) NOT NULL COMMENT '安全组ID',
  `protocol` varchar(64) DEFAULT NULL COMMENT '协议类型,取值：tcp ，udp ，icmp，all，为all类型时，PortRange范围-1/-1',
  `port_range` varchar(64) DEFAULT NULL COMMENT '端口范围,IP协议相关的端口号范围如1/80，IpProtocol不是all则不为空',
  `remote_ip_prefix` varchar(64) DEFAULT NULL COMMENT '目标网段',
  `remote_group_id` varchar(64) DEFAULT NULL COMMENT '目标安全组',
  `direction` varchar(32) DEFAULT NULL COMMENT '方向',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `ethertype` varchar(64) DEFAULT NULL COMMENT '以太网类型，IPV4，IPV6',
  `tenant_id` varchar(64) DEFAULT NULL COMMENT '租户ID',
  `user_id` varchar(32) DEFAULT NULL COMMENT '所有者用户ID',
  `provider` int(10) DEFAULT null,
  `provider_ref_id` varchar(40) DEFAULT null,
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_at` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 默认安全组
ALTER TABLE gc_security_groups ADD is_default tinyint(1) null;

-- 任务流实例表、任务流实例步骤表添加最顶层任务id  2019-06-25
ALTER TABLE `gc_work_flow_instance` ADD `topest_flow_task_id` char(64) DEFAULT NULL COMMENT '最顶层任务id，根据这个字段判断是否在同一个任务流程';
ALTER TABLE `gc_work_flow_instance_step` ADD `topest_flow_task_id` char(64) DEFAULT NULL COMMENT '最顶层任务id，根据这个字段判断是否在同一个任务流程';