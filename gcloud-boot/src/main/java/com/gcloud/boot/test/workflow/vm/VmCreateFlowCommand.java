package com.gcloud.boot.test.workflow.vm;

import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gcloud.boot.test.workflow.model.VmCreateParams;
import com.gcloud.boot.test.workflow.model.VmCreateRes;
import com.gcloud.common.util.GenIDUtil;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;

import lombok.extern.slf4j.Slf4j;
@Component
@Scope("prototype")
@Slf4j
public class VmCreateFlowCommand extends BaseWorkFlowCommand{

	@Override
	protected Object process() throws Exception {
		VmCreateParams req = (VmCreateParams)getReqParams();
		log.debug(req.getType());
		
		VmCreateRes res = new VmCreateRes();
		res.setInstanceId(GenIDUtil.genernateId("i-", "8"));
		return res;
	}

	@Override
	protected Object rollback() throws Exception {
		return null;
	}

	@Override
	protected Object timeout() throws Exception {
		return null;
	}

	@Override
	protected Class<?> getReqParamClass() {
		return VmCreateParams.class;
	}

	@Override
	protected Class<?> getResParamClass() {
		return VmCreateRes.class;
	}

}
