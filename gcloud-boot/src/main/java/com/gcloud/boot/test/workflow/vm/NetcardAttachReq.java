package com.gcloud.boot.test.workflow.vm;

public class NetcardAttachReq {
	private String instanceId;
	private String netcardId;
	public String getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	public String getNetcardId() {
		return netcardId;
	}
	public void setNetcardId(String netcardId) {
		this.netcardId = netcardId;
	}
	
	
}
