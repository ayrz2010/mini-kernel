package com.gcloud.boot.test.workflow;

import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gcloud.core.workflow.core.BaseWorkFlowCommand;
@Component
@Scope("prototype")
public class NestWorkflowOneCommand  extends BaseWorkFlowCommand {

	@Override
	protected Object process() throws Exception {
		// TODO Auto-generated method stub
		System.out.println("NestWorkflowOneCommand process....thread id:" + Thread.currentThread().getId());
		return null;
	}
	
	@Override
	protected Object rollback() throws Exception {
		// TODO Auto-generated method stub
		System.out.println("NestWorkflowOneCommand rollbacking....thread id:" + Thread.currentThread().getId());
		return null;
	}

	@Override
	protected Object timeout() throws Exception {
		// TODO Auto-generated method stub
		System.out.println("NestWorkflowOneCommand timeout....thread id:" + Thread.currentThread().getId());
		return null;
	}

	@Override
	protected Class<?> getReqParamClass() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Class<?> getResParamClass() {
		// TODO Auto-generated method stub
		return null;
	}

}
