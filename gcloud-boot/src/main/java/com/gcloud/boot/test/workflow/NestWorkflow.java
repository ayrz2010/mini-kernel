package com.gcloud.boot.test.workflow;

import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gcloud.boot.test.workflow.model.NestWorkflowReq;
import com.gcloud.core.workflow.core.BaseWorkFlows;
@Component
@Scope("prototype")
public class NestWorkflow extends BaseWorkFlows{

	@Override
	public String getFlowTypeCode() {
		// TODO Auto-generated method stub
		return "NestWorkflow";
	}

	@Override
	public void process() {
		// TODO Auto-generated method stub
		System.out.println("NestWorkflow start...");
	}

	@Override
	protected Class<?> getReqParamClass() {
		// TODO Auto-generated method stub
		return NestWorkflowReq.class;
	}

	@Override
	public Object preProcess() {
		return null;
	}
	@Override
	public String getBatchFiled() {
		// TODO Auto-generated method stub
		return "batch";
	}
}
