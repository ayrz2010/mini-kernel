package com.gcloud.boot.test.workflow;

import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gcloud.boot.test.workflow.model.TestRepeatFlowTwoReq;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;

@Component
@Scope("prototype")
public class TestRepeatFlowTwoCommand extends BaseWorkFlowCommand{

	@Override
	protected Object process() throws Exception {
		// TODO Auto-generated method stub
		System.out.println("TestRepeatFlowTwoCommand");
		TestRepeatFlowTwoReq req = (TestRepeatFlowTwoReq)getReqParams();
		System.out.println(req.getRepeatParams().getNetCardName() + req.getRepeatParams().getType());
		return null;
	}

	@Override
	protected Object rollback() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Object timeout() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Class<?> getReqParamClass() {
		// TODO Auto-generated method stub
		return TestRepeatFlowTwoReq.class;
	}

	@Override
	protected Class<?> getResParamClass() {
		// TODO Auto-generated method stub
		return null;
	}

}
