package com.gcloud.boot.test.workflow.rest;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gcloud.boot.test.workflow.TestRepeatFlow;

@RestController
@RequestMapping("/")
public class TestRepeatFlowController {
	@RequestMapping(value = "/test/repeatworkflow")
	public void api(){
		
		TestRepeatFlow flow = new TestRepeatFlow();
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			flow.execute(params);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
