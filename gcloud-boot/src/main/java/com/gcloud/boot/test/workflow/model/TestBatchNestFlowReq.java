package com.gcloud.boot.test.workflow.model;

import java.util.List;

public class TestBatchNestFlowReq {
	private int num;
	private List<Cards> repeatParams;

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public List<Cards> getRepeatParams() {
		return repeatParams;
	}

	public void setRepeatParams(List<Cards> repeatParams) {
		this.repeatParams = repeatParams;
	}
	
}
