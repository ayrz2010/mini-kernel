package com.gcloud.boot.test.workflow;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gcloud.core.workflow.core.BaseWorkFlows;
@Component
@Scope("prototype")
public class NestTwoWorkflow  extends BaseWorkFlows{
	@Override
	public String getFlowTypeCode() {
		// TODO Auto-generated method stub
		return "NestTwoWorkflow";
	}

	@Override
	public void process() {
		// TODO Auto-generated method stub
		System.out.println("NestTwoWorkflow start...");
	}

	@Override
	protected Class<?> getReqParamClass() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object preProcess() {
		// TODO Auto-generated method stub
		return null;
	}
}
