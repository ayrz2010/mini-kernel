package com.gcloud.boot.test.workflow.model;

import java.util.List;

public class NestWorkflowThreeCommandReq {
	private List<TestBatchNestFlowResCommandRes> ress;
	private String testSkip;

	public List<TestBatchNestFlowResCommandRes> getRess() {
		return ress;
	}

	public void setRess(List<TestBatchNestFlowResCommandRes> ress) {
		this.ress = ress;
	}

	public String getTestSkip() {
		return testSkip;
	}

	public void setTestSkip(String testSkip) {
		this.testSkip = testSkip;
	}
	
}
