package com.gcloud.boot.test.workflow.model;

public class RepeatRes {
	private String netCardName;
	private String type;
	public String getNetCardName() {
		return netCardName;
	}
	public void setNetCardName(String netCardName) {
		this.netCardName = netCardName;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	
}
