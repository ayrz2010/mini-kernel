package com.gcloud.boot.test.workflow.model;

public class TestBatchNestFlowResCommandRes {
	private boolean rollback;

	public boolean isRollback() {
		return rollback;
	}

	public void setRollback(boolean rollback) {
		this.rollback = rollback;
	}
	
}
