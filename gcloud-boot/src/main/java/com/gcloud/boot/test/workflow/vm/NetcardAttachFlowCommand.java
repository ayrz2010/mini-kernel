package com.gcloud.boot.test.workflow.vm;

import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gcloud.core.workflow.core.BaseWorkFlowCommand;

import lombok.extern.slf4j.Slf4j;

@Component
@Scope("prototype")
@Slf4j
public class NetcardAttachFlowCommand extends BaseWorkFlowCommand{
	@Override
	protected Object process() throws Exception {
		NetcardAttachReq req = (NetcardAttachReq)getReqParams();
		log.debug("NetcardAttachFlowCommand instanceid:" + req.getInstanceId() + ",netcardid:" + req.getNetcardId());
		return null;
	}

	@Override
	protected Object rollback() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Object timeout() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Class<?> getReqParamClass() {
		// TODO Auto-generated method stub
		return NetcardAttachReq.class;
	}

	@Override
	protected Class<?> getResParamClass() {
		// TODO Auto-generated method stub
		return null;
	}

}
