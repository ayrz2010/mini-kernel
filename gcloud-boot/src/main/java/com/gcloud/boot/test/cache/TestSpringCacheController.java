package com.gcloud.boot.test.cache;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gcloud.core.service.SpringUtil;
import com.gcloud.core.workflow.entity.WorkFlowTemplate;
import com.gcloud.core.workflow.mng.IWorkFlowTemplateMng;

@RestController
public class TestSpringCacheController {
	@RequestMapping(value = "/test/springcache",method = RequestMethod.GET)
    public void lockTest(){
		IWorkFlowTemplateMng mng = (IWorkFlowTemplateMng)SpringUtil.getBean("workFlowTemplateMng");
		WorkFlowTemplate template = mng.findById(10000);
		System.out.println(template.getExecCommand() + template.getFlowTypeCode());
    }
}
