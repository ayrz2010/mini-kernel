package com.gcloud.boot.test.mq;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

//@Configuration
public class RabbitConfig {
	//@Value("${service.controller}")
	public String queueName;
    //@Bean
    public Queue queue() {
    	System.out.println("boot");
    	//return new Queue("hello");
        return new Queue(queueName);
    }
    /*
    //构造ConnectionFactory 
    @Bean
    @ConfigurationProperties(prefix = "spring.rabbitmq")
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory factory = new CachingConnectionFactory();
        factory.setPublisherConfirms(Boolean.TRUE);
        return factory;
    }

    @Bean
    @Scope("prototype")
    public RabbitTemplate rabbitTemplate() {
        return new RabbitTemplate(connectionFactory());
    }
    */
}