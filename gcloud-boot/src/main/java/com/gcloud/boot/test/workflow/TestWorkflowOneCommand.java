package com.gcloud.boot.test.workflow;

import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gcloud.boot.test.workflow.model.TestWorkflowOneReq;
import com.gcloud.boot.test.workflow.model.TestWorkflowOneRes;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;

@Component
@Scope("prototype")
public class TestWorkflowOneCommand extends BaseWorkFlowCommand {

	@Override
	protected Object process() throws Exception {
		System.out.println("1 process....thread id:" + Thread.currentThread().getId());
		TestWorkflowOneReq req = (TestWorkflowOneReq)getReqParams();
//		System.out.println("req: name=" + req.getName());
		
		if(req.getName().equals("alice"))
		{
			throw new Exception("error test");
		}
		
		TestWorkflowOneRes res = new TestWorkflowOneRes();
		res.setHabbit("movie");
		return res;
	}

	@Override
	protected Object rollback() throws Exception {
		System.out.println("1 rollbacking....thread id:" + Thread.currentThread().getId());
		return null;
	}

	@Override
	protected Object timeout() throws Exception {
		System.out.println("1 timeout....thread id:" + Thread.currentThread().getId());
		TestWorkflowOneRes res = (TestWorkflowOneRes)getResParams();
//		System.out.println("req: name=" + res.getHabbit());
		
		if(res.getHabbit().equals("movie"))
		{
			throw new Exception("timeout error test");
		}
		return null;
	}

	@Override
	protected Class<?> getReqParamClass() {
		// TODO Auto-generated method stub
		return TestWorkflowOneReq.class;
	}

	@Override
	protected Class<?> getResParamClass() {
		// TODO Auto-generated method stub
		return TestWorkflowOneRes.class;
	}

}
