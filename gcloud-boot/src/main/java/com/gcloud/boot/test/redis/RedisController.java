package com.gcloud.boot.test.redis;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.cache.redis.lock.util.LockUtil;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class RedisController {
	
    @RequestMapping(value = "/test/cache",method = RequestMethod.GET)
    public void cacheTest(){
    	log.debug("cacheTest request");
    	Map<String, String> testObj = new HashMap<String, String>();
    	testObj.put("1", "test1中文");
    	testObj.put("2", "test2");
        
        CacheContainer.getInstance().put(CacheType.ACTIVE_INFO, "activecode", "中文HHJJJUGGGFFGGGG");
        
        CacheContainer.getInstance().put(CacheType.ACTIVE_INFO, "product", "gcloud7.0中文版");
        
        CacheContainer.getInstance().put(CacheType.NODE_INFO, testObj);
        
        System.out.println(CacheContainer.getInstance().getString(CacheType.ACTIVE_INFO, "activecode") + ","
        		+ CacheContainer.getInstance().getString(CacheType.ACTIVE_INFO, "product"));
        
        System.out.println(CacheContainer.getInstance().getString(CacheType.NODE_INFO, "1") + "," 
        		+ CacheContainer.getInstance().getString(CacheType.NODE_INFO, "2"));
    }
    
    @RequestMapping(value = "/test/lock",method = RequestMethod.GET)
    public void lockTest(){
        
        String lockId = LockUtil.spinLock("alice", 5, 10, "alice_get_lock");
        System.out.println("lockId:" + lockId);
        LockUtil.releaseSpinLock("alice", lockId);
    }
}
