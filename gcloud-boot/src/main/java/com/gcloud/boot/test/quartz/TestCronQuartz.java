package com.gcloud.boot.test.quartz;

import org.quartz.DailyTimeIntervalScheduleBuilder;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.ScheduleBuilder;
import org.quartz.TimeOfDay;
import org.quartz.Trigger;
import org.springframework.stereotype.Component;

import com.gcloud.core.quartz.GcloudJobBean;
import com.gcloud.core.quartz.annotation.QuartzTimer;

//@Component
//@QuartzTimer
public class TestCronQuartz extends GcloudJobBean{

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		// TODO Auto-generated method stub
		System.out.println("TestCronQuartz run .... ");
	}
	
	public ScheduleBuilder<? extends Trigger> scheduleBuilder(){
		return DailyTimeIntervalScheduleBuilder.dailyTimeIntervalSchedule()
	    .startingDailyAt(TimeOfDay.hourAndMinuteOfDay(9, 0)) //第天9：00开始
	    .endingDailyAt(TimeOfDay.hourAndMinuteOfDay(19, 0)) //19：00 结束 
//	    .onDaysOfTheWeek(MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY) //周一至周五执行
	    .withIntervalInMinutes(60) //每间隔1分钟执行一次
	    .withRepeatCount(100); //最多重复100次（实际执行100+1次）
	}
}
