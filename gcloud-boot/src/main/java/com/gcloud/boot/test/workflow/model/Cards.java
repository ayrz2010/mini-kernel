package com.gcloud.boot.test.workflow.model;

public class Cards {
	private String cardName;
	private String subNet;
	
	public String getCardName() {
		return cardName;
	}
	public void setCardName(String cardName) {
		this.cardName = cardName;
	}
	public String getSubNet() {
		return subNet;
	}
	public void setSubNet(String subNet) {
		this.subNet = subNet;
	}
}
