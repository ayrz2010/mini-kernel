package com.gcloud.boot.test.workflow.model;

import java.util.List;

public class CreateAttachNetCardFlowReq {
	private String type;
	private String instanceId;
	private List<Cards> cards;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public List<Cards> getCards() {
		return cards;
	}
	public void setCards(List<Cards> cards) {
		this.cards = cards;
	}
	public String getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
}
