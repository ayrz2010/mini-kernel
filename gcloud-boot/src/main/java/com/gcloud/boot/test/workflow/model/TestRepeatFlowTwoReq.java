package com.gcloud.boot.test.workflow.model;

public class TestRepeatFlowTwoReq {
	private RepeatRes repeatParams;

	public RepeatRes getRepeatParams() {
		return repeatParams;
	}

	public void setRepeatParams(RepeatRes repeatParams) {
		this.repeatParams = repeatParams;
	}
	
	
}
