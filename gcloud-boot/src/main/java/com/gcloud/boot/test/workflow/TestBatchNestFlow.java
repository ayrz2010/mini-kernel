package com.gcloud.boot.test.workflow;

import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gcloud.boot.test.workflow.model.TestBatchNestFlowReq;
import com.gcloud.core.workflow.core.BaseWorkFlows;
@Component
@Scope("prototype")
public class TestBatchNestFlow  extends BaseWorkFlows{

	@Override
	public String getFlowTypeCode() {
		// TODO Auto-generated method stub
		return "TestBatchNestFlow";//this.getClass().getSimpleName();
	}

	@Override
	public void process() {
		// TODO Auto-generated method stub
		System.out.println(this.getClass().getSimpleName());;
	}

	@Override
	protected Class<?> getReqParamClass() {
		// TODO Auto-generated method stub
		return TestBatchNestFlowReq.class;
	}

	@Override
	public Object preProcess() {
		return null;
	}
	
	@Override
	public String getBatchFiled() {
		// TODO Auto-generated method stub
		return "num";
	}

}
