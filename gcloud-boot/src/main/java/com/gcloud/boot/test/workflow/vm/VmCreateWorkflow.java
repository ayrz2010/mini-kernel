package com.gcloud.boot.test.workflow.vm;

import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gcloud.boot.test.workflow.model.VmCreateParams;
import com.gcloud.core.workflow.core.BaseWorkFlows;

@Component
@Scope("prototype")
public class VmCreateWorkflow extends BaseWorkFlows{

	@Override
	public String getFlowTypeCode() {
		return "VmCreateWorkflow";
	}

	@Override
	public void process() {
		
	}

	@Override
	protected Class<?> getReqParamClass() {
		return VmCreateParams.class;
	}
	
	@Override
	public String getBatchFiled() {
		// TODO Auto-generated method stub
		return "vmNum";
	}

	@Override
	public Object preProcess() {
		return null;
	}
	
	@Override
	public boolean judgeExecute() {
		// 可以在workflow这里实现这个方法，根据参数决定返回true还是false
		return super.judgeExecute();
	}
}
