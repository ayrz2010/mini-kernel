package com.gcloud.boot.test.workflow;

import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gcloud.boot.test.workflow.model.TestWorkflowOneReq;
import com.gcloud.boot.test.workflow.model.TestWorkflowOneRes;
import com.gcloud.boot.test.workflow.model.TestWorkflowTwoReq;
import com.gcloud.boot.test.workflow.model.TestWorkflowTwoRes;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;
@Component
@Scope("prototype")
public class TestWorkflowTwoCommand extends BaseWorkFlowCommand{

	@Override
	protected Object process() throws Exception {
		// TODO Auto-generated method stub
		System.out.println("2 process....thread id:" + Thread.currentThread().getId());
		TestWorkflowTwoReq req = (TestWorkflowTwoReq)getReqParams();
		System.out.println("req: habbit=" + req.getHabbit());
		
		TestWorkflowTwoRes res = new TestWorkflowTwoRes();
		res.setFood("apple");
		return res;
	}

	@Override
	protected Object rollback() throws Exception {
		// TODO Auto-generated method stub
		System.out.println("2 rollbacking....thread id:" + Thread.currentThread().getId());
		return null;
	}

	@Override
	protected Object timeout() throws Exception {
		// TODO Auto-generated method stub
		System.out.println("2 timeout....thread id:" + Thread.currentThread().getId());
		TestWorkflowTwoRes res = (TestWorkflowTwoRes)getResParams();
		System.out.println("res: food=" + res.getFood());
		
		if(res.getFood().equals("apple1"))
		{
			throw new Exception("timeout error test");
		}
		return null;
	}

	@Override
	protected Class<?> getReqParamClass() {
		// TODO Auto-generated method stub
		return TestWorkflowTwoReq.class;
	}

	@Override
	protected Class<?> getResParamClass() {
		// TODO Auto-generated method stub
		return TestWorkflowTwoRes.class;
	}

}
