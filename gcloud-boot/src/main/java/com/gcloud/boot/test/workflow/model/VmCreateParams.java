package com.gcloud.boot.test.workflow.model;

import java.util.List;

public class VmCreateParams {
	private String type;
	private int vmNum;
	private List<Cards> cards;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getVmNum() {
		return vmNum;
	}
	public void setVmNum(int vmNum) {
		this.vmNum = vmNum;
	}
	public List<Cards> getCards() {
		return cards;
	}
	public void setCards(List<Cards> cards) {
		this.cards = cards;
	}
}
