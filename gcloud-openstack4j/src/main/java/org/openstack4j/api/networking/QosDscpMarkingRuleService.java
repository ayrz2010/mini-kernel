package org.openstack4j.api.networking;

import org.openstack4j.common.RestService;
import org.openstack4j.model.network.QosDscpMarkingRule;

/**
 *
 * @author yaowj
 */
public interface QosDscpMarkingRuleService extends RestService {

    QosDscpMarkingRule create(String policyId, Integer dscpMark);
	

}
