package org.openstack4j.api.networking;

import org.openstack4j.common.RestService;
import org.openstack4j.model.common.ActionResponse;
import org.openstack4j.model.network.QosBandwidthLimitRule;
import org.openstack4j.model.network.QosDirection;

/**
 * @author yaowj
 */
public interface QosBandwidthLimitRuleService extends RestService {

    QosBandwidthLimitRule create(String policyId, Integer maxKbps, Integer maxBurstKbps, QosDirection direction);

    QosBandwidthLimitRule update(String policyId, String ruleId, Integer maxKbps, Integer maxBurstKbps, QosDirection direction);

    ActionResponse delete(String policyId, String ruleId);

}
