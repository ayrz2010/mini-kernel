package org.openstack4j.model.network;

import org.openstack4j.common.Buildable;
import org.openstack4j.model.network.builder.QosMinimumBandwidthRuleBuilder;

/**
 * Created by yaowj on 2018/10/26.
 */
public interface QosMinimumBandwidthRule extends QosRule, Buildable<QosMinimumBandwidthRuleBuilder> {

    Integer getMinKbps();
    QosDirection getDirection();

}
