package org.openstack4j.model.network;

/**
 * Created by yaowj on 2018/10/26.
 */
public interface QosBandwidthLimitRuleInfo extends QosBandwidthLimitRule, QosRuleInfo {

}
