package org.openstack4j.model.network;

import org.openstack4j.model.ModelEntity;

/**
 * Created by yaowj on 2018/10/26.
 */
public interface QosRule extends ModelEntity {

    String getId();

}
