package org.openstack4j.model.network.builder;

import org.openstack4j.common.Buildable.Builder;
import org.openstack4j.model.network.QosPolicy;

public interface QosPolicyBuilder extends Builder<QosPolicyBuilder, QosPolicy> {

	QosPolicyBuilder id(String id);

	QosPolicyBuilder name(String name);

	QosPolicyBuilder description(String description);

	QosPolicyBuilder projectId(String projectId);

	QosPolicyBuilder shared(Boolean isShared);

	QosPolicyBuilder isDefault(Boolean isDefault);

}
