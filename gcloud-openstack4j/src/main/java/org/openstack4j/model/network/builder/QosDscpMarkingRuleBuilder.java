package org.openstack4j.model.network.builder;

import org.openstack4j.common.Buildable.Builder;
import org.openstack4j.model.network.QosDscpMarkingRule;

/**
 * Created by yaowj on 2018/10/26.
 */
public interface QosDscpMarkingRuleBuilder extends Builder<QosDscpMarkingRuleBuilder, QosDscpMarkingRule> {

    QosDscpMarkingRuleBuilder id(String id);
    QosDscpMarkingRuleBuilder dscpMarking(Integer dscpMarking);

}
