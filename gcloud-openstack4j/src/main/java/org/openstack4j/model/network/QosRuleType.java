package org.openstack4j.model.network;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Created by yaowj on 2018/10/26.
 */
public enum QosRuleType {
    BANDWIDTH_LIMIT,
    DSCP_MARKING,
    MINIMUM_BANDWIDTH;

    @JsonCreator
    public static QosRuleType forValue(String value) {
        if (value != null) {
            for (QosRuleType s : QosRuleType.values()) {
                if (s.name().equalsIgnoreCase(value))
                    return s;
            }
        }
        return null;
    }

    @JsonValue
    public String toJson() {
        return name().toLowerCase();
    }

}
