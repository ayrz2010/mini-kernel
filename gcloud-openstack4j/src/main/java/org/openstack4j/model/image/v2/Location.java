package org.openstack4j.model.image.v2;

import org.openstack4j.model.ModelEntity;
import org.openstack4j.openstack.common.Metadata;

/**
 * Created by yaowj on 2018/11/22.
 */
public interface Location extends ModelEntity {

    String getUrl();
    Metadata getMetadata();

}
