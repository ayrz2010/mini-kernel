package org.openstack4j.model.storage.block.builder;

import org.openstack4j.common.Buildable.Builder;
import org.openstack4j.model.storage.block.Connector;

public interface ConnectorBuilder extends Builder<ConnectorBuilder, Connector> {

	ConnectorBuilder platform(String platform);

	ConnectorBuilder host(String host);

	ConnectorBuilder ip(String ip);

	ConnectorBuilder doLocalAttach(boolean doLocalAttach);

	ConnectorBuilder multipath(boolean multipath);

	ConnectorBuilder initiator(String initiator);

	ConnectorBuilder osType(String osType);
}
