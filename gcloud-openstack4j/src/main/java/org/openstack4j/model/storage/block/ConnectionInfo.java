package org.openstack4j.model.storage.block;

import java.util.Map;

import org.openstack4j.model.ModelEntity;

public interface ConnectionInfo extends ModelEntity {

	String getDriverVolumeType();

	Map<String, String> getData();

}
