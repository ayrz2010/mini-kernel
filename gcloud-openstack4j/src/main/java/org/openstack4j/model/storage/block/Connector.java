package org.openstack4j.model.storage.block;

import org.openstack4j.common.Buildable;
import org.openstack4j.model.ModelEntity;
import org.openstack4j.model.storage.block.builder.ConnectorBuilder;

public interface Connector extends ModelEntity, Buildable<ConnectorBuilder> {

	String getPlatform();

	String getHost();

	boolean getDoLocalAttach();

	String getIp();

	String getOsType();

	boolean getMultipath();

	String getInitiator();
}
