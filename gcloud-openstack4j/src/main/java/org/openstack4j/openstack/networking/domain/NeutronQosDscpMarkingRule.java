package org.openstack4j.openstack.networking.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.google.common.base.MoreObjects;
import org.openstack4j.model.network.QosDscpMarkingRule;
import org.openstack4j.model.network.builder.QosDscpMarkingRuleBuilder;

/**
 * Created by yaowj on 2018/10/29.
 */
@JsonRootName("dscp_marking_rule")
@JsonIgnoreProperties(ignoreUnknown = true)
public class NeutronQosDscpMarkingRule implements QosDscpMarkingRule {

    private String id;
    @JsonProperty("dscp_mark")
    private Integer dscpMark;

    public String getId() {
        return id;
    }

    @Override
    public Integer getDscpMark() {
        return dscpMark;
    }


    public NeutronQosDscpMarkingRule() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).omitNullValues().add("id", id).add("dscp_mark", dscpMark)
                .toString();
    }

    @Override
    public QosDscpMarkingRuleConcreteBuilder toBuilder() {
        return new QosDscpMarkingRuleConcreteBuilder(this);
    }

    public static QosDscpMarkingRuleConcreteBuilder builder() {
        return new QosDscpMarkingRuleConcreteBuilder();
    }

    public static class QosDscpMarkingRuleConcreteBuilder implements QosDscpMarkingRuleBuilder {
        private NeutronQosDscpMarkingRule m;

        public QosDscpMarkingRuleConcreteBuilder() {
            this(new NeutronQosDscpMarkingRule());
        }

        public QosDscpMarkingRuleConcreteBuilder(NeutronQosDscpMarkingRule rule) {
            this.m = rule;
        }

        @Override
        public QosDscpMarkingRule build() {
            return m;
        }

        @Override
        public QosDscpMarkingRuleConcreteBuilder from(QosDscpMarkingRule in) {
            m = (NeutronQosDscpMarkingRule) in;
            return this;
        }

        @Override
        public QosDscpMarkingRuleBuilder id(String id) {
            m.id = id;
            return this;
        }

        @Override
        public QosDscpMarkingRuleBuilder dscpMarking(Integer dscpMarking) {
            m.dscpMark = dscpMarking;
            return this;
        }
    }
}
