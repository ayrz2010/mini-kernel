package org.openstack4j.openstack.networking.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import org.openstack4j.model.network.QosDirection;
import org.openstack4j.model.network.QosRuleType;

/**
 * Created by yaowj on 2018/10/29.
 */
@JsonRootName("minimum_bandwidth_rule")
@JsonIgnoreProperties(ignoreUnknown = true)
public class NeutronQosMinimumBandwidthRuleInfo implements NeutronQosRuleInfo{

    private static final long serialVersionUID = 1L;

    private String id;
    @JsonProperty("min_kbps")
    private Integer minKbps;
    private QosDirection direction;
    private QosRuleType type = QosRuleType.BANDWIDTH_LIMIT;

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getMinKbps() {
        return minKbps;
    }

    public void setMinKbps(Integer minKbps) {
        this.minKbps = minKbps;
    }

    public QosDirection getDirection() {
        return direction;
    }

    public void setDirection(QosDirection direction) {
        this.direction = direction;
    }

    @Override
    public QosRuleType getType() {
        return type;
    }

    public void setType(QosRuleType type) {
        this.type = type;
    }
}
