package org.openstack4j.openstack.image.v2.domain;

import org.openstack4j.model.image.v2.Location;
import org.openstack4j.openstack.common.Metadata;

/**
 * Created by yaowj on 2018/11/22.
 */
public class GlanceLocation implements Location {

    private String url;
    private Metadata metadata;

    @Override
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }
}
