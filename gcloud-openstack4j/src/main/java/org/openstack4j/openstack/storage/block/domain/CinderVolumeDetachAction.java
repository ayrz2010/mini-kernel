package org.openstack4j.openstack.storage.block.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import org.openstack4j.model.ModelEntity;

/**
 * Created by yaowj on 2018/11/7.
 */
@JsonRootName("os-detach")
public class CinderVolumeDetachAction implements ModelEntity {

    private static final long serialVersionUID = 1L;

    @JsonProperty("attachment_id")
    private String attachmentId;

    public CinderVolumeDetachAction(String attachmentId) {
        this.attachmentId = attachmentId;
    }

    public String getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(String attachmentId) {
        this.attachmentId = attachmentId;
    }
}
