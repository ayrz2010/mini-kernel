package org.openstack4j.openstack.storage.block.domain;

import org.openstack4j.model.ModelEntity;
import org.openstack4j.model.storage.block.Connector;

import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName("os-initialize_connection")
public class CinderInitializeConnection implements ModelEntity {

	private static final long serialVersionUID = 1L;

	private Connector connector;

	public CinderInitializeConnection(Connector connector) {
		this.connector = connector;
	}

	public Connector getConnector() {
		return connector;
	}

	public void setConnector(Connector connector) {
		this.connector = connector;
	}

}
