package org.openstack4j.openstack.storage.block.domain;

import java.util.Map;

import org.openstack4j.model.storage.block.ConnectionInfo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CinderConnectionInfo implements ConnectionInfo {

	private static final long serialVersionUID = 1L;

	@JsonProperty("driver_volume_type")
	private String driverVolumeType;

	private Map<String, String> data;

	@Override
	public String getDriverVolumeType() {
		return driverVolumeType;
	}

	@Override
	public Map<String, String> getData() {
		return data;
	}

}
